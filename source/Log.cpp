// Log.cpp: implementation of the CLog class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Log.h"
#include "shlwapi.h"
#pragma comment(lib, "shlwapi.lib")

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CLog::CLog()
{
	dir = "";
}

CLog::~CLog()
{
}

void CLog::Message(CString str)
{
	CSingleLock Lock(&CS);	
	Lock.Lock();
	if (dir.GetLength() > 0)
	{
		CFile theFile;
		if (theFile.Open(dir, CFile::modeNoTruncate | CFile::modeWrite))
		{ 
			theFile.SeekToEnd();
			str = CTime::GetCurrentTime().Format("%Y-%m-%d %H:%M:%S:") + str;
			str += "\r\n";
			theFile.Write(str.GetBuffer(), 2 * str.GetLength());
			theFile.Close();
		}
	}
}

void CLog::NewLog(CString winCaption)
{
	BOOL status = FALSE;
	
	dir = "C:\\CTCReviewerLog";
	if (!PathIsDirectory(dir))
	{
		if (!CreateDirectory(dir, NULL))
		{
			dir = "";
		}
	}
	
	if (dir.GetLength() > 0)
	{
		dir += "\\LOG";
		dir += CTime::GetCurrentTime().Format(_T("%Y%m%d%H%M%S"));
		dir += ".LOG";
		CFile theFile;
		if (theFile.Open(dir, CFile::modeCreate | CFile::modeWrite))
		{
			theFile.Close();
		}
		msg.Format(_T("Launched %s Program."), winCaption);
		Message(msg);
	}
}

