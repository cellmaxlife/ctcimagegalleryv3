#pragma once
#include <vector>
#include "BlobData.h"
#include "RGNData.h"

using namespace std;

class CColorStain
{
public:
	CColorStain();
	virtual ~CColorStain();

	CRGNData *m_RedRgnPtr;
	CBlobData *m_RedBlob;
	vector<CBlobData *> m_BlueBlobs;
	CBlobData *m_GreenBlob;
};