#pragma once

#include <afxwin.h>

class CSingleRegionImageData : public CObject
{
public:
	DECLARE_SERIAL(CSingleRegionImageData);
	CSingleRegionImageData();
	virtual ~CSingleRegionImageData();

	void Serialize(CArchive& archive);

	int m_HitIndex;
	int m_RegionX0Pos;
	int m_RegionY0Pos;
	int m_RegionWidth;
	int m_RegionHeight;
	unsigned short* m_RedRegionImage;
	unsigned short* m_GreenRegionImage;
	unsigned short* m_BlueRegionImage;
};