#include "stdafx.h"
#include "BlobData.h"

CBlobData::CBlobData(int width, int height)
{
	m_Width = width;
	m_Height = height;
	m_Boundary = new vector<unsigned int>();
	m_Map = new unsigned char[width * height];
	memset(m_Map, 0, sizeof(unsigned char) * width * height);
	m_Rect.left = 0;
	m_Rect.right = 0;
	m_Rect.top = 0;
	m_Rect.bottom = 0;
	m_PixelCount = 0;
	m_Threshold = 0;
	m_MaxIntensity = 0;
	m_MaxIntenX = 0;
	m_MaxIntenY = 0;
	m_AverageIntensity = 0;
	m_IntensityRange = 0;
}

CBlobData::~CBlobData()
{
	m_Boundary->clear();
	delete m_Boundary;
	if (m_Map != NULL)
		delete[] m_Map;
}

void CBlobData::CopyData(CBlobData *blob)
{
	m_Rect.left = blob->m_Rect.left;
	m_Rect.top = blob->m_Rect.top;
	m_Rect.right = blob->m_Rect.right;
	m_Rect.bottom = blob->m_Rect.bottom;
	m_MaxIntensity = blob->m_MaxIntensity;
	m_Threshold = blob->m_Threshold;
	m_PixelCount = blob->m_PixelCount;
	m_Boundary->clear();
	for (int i = 0; i < (int)blob->m_Boundary->size(); i++)
	{
		m_Boundary->push_back((*blob->m_Boundary)[i]);
	}
	memcpy(m_Map, blob->m_Map, sizeof(unsigned char) * blob->m_Width * blob->m_Height);
	m_MaxIntenX = blob->m_MaxIntenX;
	m_MaxIntenY = blob->m_MaxIntenY;
	m_AverageIntensity = blob->m_AverageIntensity;
	m_IntensityRange = blob->m_IntensityRange;
}