#include "stdafx.h"
#include "HitFindingOperation.h"
#include <string.h>
#include "CTCParams.h"
#include "VersionNumber.h"
#include <math.h>

#define DEFAULT_MIN_BLOBPIXELS	20
#define MAX_NUM_BLOB_PER_FRAME	10
#define SAMPLEFRAMEWIDTH 100
#define SAMPLEFRAMEHEIGHT 100
#define PI 3.14159265

CHitFindingOperation::CHitFindingOperation()
{
	m_MinBlobPixelCount = 100;
	m_MaxBlobPixelCount = 900;
}

CHitFindingOperation::~CHitFindingOperation()
{
}

void CHitFindingOperation::ProcessOneROI(CCTCParams *params, CRGNData *hitData, vector<CRGNData *> *hitsFound)
{
	hitData->FreeBlobList(RED_COLOR);
	hitData->FreeBlobList(GREEN_COLOR);
	hitData->FreeBlobList(BLUE_COLOR);
	hitData->FreeBlobList(RB_COLORS);
	hitData->FreeBlobList(GB_COLORS);
	hitData->m_RedFrameMax = hitData->GetCPI(RED_COLOR) + hitData->GetContrast(RED_COLOR);
	hitData->m_GreenFrameMax = hitData->GetCPI(GREEN_COLOR) + hitData->GetContrast(GREEN_COLOR);
	hitData->m_BlueFrameMax = hitData->GetCPI(BLUE_COLOR) + hitData->GetContrast(BLUE_COLOR);

	vector<CColorStain *> cellCandidates;
	GetBlobs(params, hitData, &cellCandidates);
#if 1 // ==0 for display blobs in HitViewer Only
	if (cellCandidates.size() == 0)
	{
		return;
	}

	int width = hitData->GetWidth();
	int height = hitData->GetHeight();
	unsigned char *greenRingMap = new unsigned char[sizeof(unsigned char) * width * height];
	unsigned char *temp = new unsigned char[width * height];

	for (int m = 0; m < (int)cellCandidates.size(); m++)
	{
		int redPixelCounts = 0;
		int greenPixelCounts = 0;
		int bluePixelCounts = 0;
		int blueRedIntersectPixelCounts = 0;

		CRGNData *ptr = cellCandidates[m]->m_RedRgnPtr;
		cellCandidates[m]->m_RedRgnPtr = NULL;
		CBlobData *rptr = cellCandidates[m]->m_RedBlob;
		cellCandidates[m]->m_RedBlob = NULL;

		unsigned short *redImg = hitData->GetImage(RED_COLOR);
		unsigned short *greenImg = hitData->GetImage(GREEN_COLOR);
		unsigned short *blueImg = hitData->GetImage(BLUE_COLOR);

		ptr->SetBoundingBox(rptr->m_Rect.left, rptr->m_Rect.top, rptr->m_Rect.right, rptr->m_Rect.bottom);
		ptr->m_AspectRatio = GetAspectRatio(rptr, ptr);
		hitData->m_AspectRatio = ptr->m_AspectRatio;
		hitData->m_LongAxisLength = ptr->m_LongAxisLength;

		for (int i = 0; i < height; i++)
		{
			for (int j = 0; j < width; j++)
			{
				if (rptr->m_Map[width * i + j] == (unsigned char)255) 
				{
					int redValue = (int)redImg[width * i + j];
					ptr->m_RedAverage += redValue;
					if (redValue > ptr->m_RedMax)
						ptr->m_RedMax = redValue;
					redPixelCounts++;
				}
			}
		}

		if (redPixelCounts > 0)
			ptr->m_RedAverage /= redPixelCounts;
		else
			ptr->m_RedAverage = 0;
		ptr->m_RedFrameMax = ptr->m_RedMax;

		if (cellCandidates[m]->m_GreenBlob != NULL)
		{
			for (int i = 0; i < height; i++)
			{
				for (int j = 0; j < width; j++)
				{
					if ((cellCandidates[m]->m_GreenBlob->m_Map[width * i + j] == (unsigned char)255) &&
						(NeighborhoodON(rptr, i, j)))
					{
						int greenValue = (int)greenImg[width * i + j];
						ptr->m_GreenAverage += greenValue;
						if (greenValue > ptr->m_GreenMax)
							ptr->m_GreenMax = greenValue;
						greenPixelCounts++;
					}
				}
			}
		}

		ptr->m_GreenFrameMax = ptr->m_GreenMax;

		for (int k = 0; k < (int)cellCandidates[m]->m_BlueBlobs.size(); k++)
		{
			CBlobData *blueBlob = cellCandidates[m]->m_BlueBlobs[k];
			for (int i = 0; i < height; i++)
			{
				for (int j = 0; j < width; j++)
				{
					if ((blueBlob->m_Map[width * i + j] == (unsigned char)255) &&
						(NeighborhoodON(rptr, i, j)))
					{
						int blueValue = (int)blueImg[width * i + j];
						ptr->m_BlueAverage += blueValue;
						if (blueValue > ptr->m_BlueMax)
							ptr->m_BlueMax = blueValue;
						blueRedIntersectPixelCounts++;
					}
				}
			}
			bluePixelCounts += blueBlob->m_PixelCount;
		}
		if (blueRedIntersectPixelCounts > 0)
		{
			ptr->m_BlueAverage /= blueRedIntersectPixelCounts;
			ptr->m_BlueValue = ptr->m_BlueAverage - hitData->GetCPI(BLUE_COLOR);
		}
		else
		{
			ptr->m_BlueAverage = 0;
			ptr->m_BlueValue = 0;
		}
		ptr->m_BlueFrameMax = ptr->m_BlueMax;

		ptr->SetPixels(RB_COLORS, blueRedIntersectPixelCounts);
		ptr->SetPixels(RED_COLOR, redPixelCounts);
		ptr->SetPixels(GREEN_COLOR, greenPixelCounts);
		ptr->SetPixels(BLUE_COLOR, bluePixelCounts);

		if (greenPixelCounts == 0)
			ptr->m_GreenAverage = 0;
		else
			ptr->m_GreenAverage /= greenPixelCounts;

		if (ptr->m_AspectRatio > params->m_CTCParams[PARAM_ASPECTRATIO15])
			ptr->m_AspectRatioScore = 15;
		else if (ptr->m_AspectRatio > params->m_CTCParams[PARAM_ASPECTRATIO10])
			ptr->m_AspectRatioScore = 10;
		else if (ptr->m_AspectRatio > params->m_CTCParams[PARAM_ASPECTRATIO5])
			ptr->m_AspectRatioScore = 5;
		else if (ptr->m_AspectRatio >= params->m_CTCParams[PARAM_ASPECTRATIO_5])
			ptr->m_AspectRatioScore = 0;
		else if (ptr->m_AspectRatio < params->m_CTCParams[PARAM_ASPECTRATIO_15])
			ptr->m_AspectRatioScore = -15;
		else if (ptr->m_AspectRatio < params->m_CTCParams[PARAM_ASPECTRATIO_10])
			ptr->m_AspectRatioScore = -10;
		else if (ptr->m_AspectRatio < params->m_CTCParams[PARAM_ASPECTRATIO_5])
			ptr->m_AspectRatioScore = -5;

		if (ptr->m_RedAverage > 0)
			ptr->m_RedValue = ptr->m_RedAverage - hitData->GetCPI(RED_COLOR);
		else
			ptr->m_RedValue = 0;
		if (ptr->m_RedValue > params->m_CTCParams[PARAM_AVERAGECK20])
			ptr->m_RedScore = 20;
		else if (ptr->m_RedValue > params->m_CTCParams[PARAM_AVERAGECK15])
			ptr->m_RedScore = 15;
		else if (ptr->m_RedValue > params->m_CTCParams[PARAM_AVERAGECK10])
			ptr->m_RedScore = 10;
		else if (ptr->m_RedValue > params->m_CTCParams[PARAM_AVERAGECK5])
			ptr->m_RedScore = 5;

		ptr->m_CellSizeValue = (float) (0.648 * ptr->m_LongAxisLength);
		if (ptr->m_CellSizeValue >= params->m_CTCParams[PARAM_CELLSIZE0])
			ptr->m_CellSizeScore = 0;
		else if (ptr->m_CellSizeValue >= params->m_CTCParams[PARAM_CELLSIZE20])
			ptr->m_CellSizeScore = 20;
		else if (ptr->m_CellSizeValue >= params->m_CTCParams[PARAM_CELLSIZE15])
			ptr->m_CellSizeScore = 15;
		else if (ptr->m_CellSizeValue >= params->m_CTCParams[PARAM_CELLSIZE10])
			ptr->m_CellSizeScore = 10;
		else if (ptr->m_CellSizeValue >= params->m_CTCParams[PARAM_CELLSIZE_5])
			ptr->m_CellSizeScore = 0;
		else if (ptr->m_CellSizeValue < params->m_CTCParams[PARAM_CELLSIZE_10])
			ptr->m_CellSizeScore = -10;
		else if (ptr->m_CellSizeValue < params->m_CTCParams[PARAM_CELLSIZE_5])
			ptr->m_CellSizeScore = -5;

		if (redPixelCounts > 0)
		{
			ptr->m_NCRatio = (float)(100.0 * ((double)ptr->GetPixels(RB_COLORS)) / ((double)ptr->GetPixels(RED_COLOR)));
			if (ptr->m_NCRatio >= params->m_CTCParams[PARAM_NCRATIO0])
				ptr->m_NCScore = -10;
			else if (ptr->m_NCRatio >= (params->m_CTCParams[PARAM_NCRATIO0] - 1))
				ptr->m_NCScore = 0;
			else if (ptr->m_NCRatio > params->m_CTCParams[PARAM_NCRATIO15])
				ptr->m_NCScore = 15;
			else if (ptr->m_NCRatio > params->m_CTCParams[PARAM_NCRATIO10])
				ptr->m_NCScore = 10;
			else if (ptr->m_NCRatio > params->m_CTCParams[PARAM_NCRATIO5])
				ptr->m_NCScore = 5;
			else if (ptr->m_NCRatio >= params->m_CTCParams[PARAM_NCRATIO_5])
				ptr->m_NCScore = 0;
			else if (ptr->m_NCRatio < params->m_CTCParams[PARAM_NCRATIO_15])
				ptr->m_NCScore = -15;
			else if (ptr->m_NCRatio < params->m_CTCParams[PARAM_NCRATIO_10])
				ptr->m_NCScore = -10;
			else if (ptr->m_NCRatio < params->m_CTCParams[PARAM_NCRATIO_5])
				ptr->m_NCScore = -5;
		}
		else
			ptr->m_NCRatio = 0.0F;

		if (ptr->m_GreenAverage > 0)
			ptr->m_GreenValue = ptr->m_GreenAverage - hitData->GetCPI(GREEN_COLOR);
		else
			ptr->m_GreenValue = 0;
		if (ptr->m_GreenValue > params->m_CTCParams[PARAM_CD45AVERAGE25])
			ptr->m_GreenScore = -25;
		else if (ptr->m_GreenValue > params->m_CTCParams[PARAM_CD45AVERAGE20])
			ptr->m_GreenScore = -20;
		else if (ptr->m_GreenValue > params->m_CTCParams[PARAM_CD45AVERAGE15])
			ptr->m_GreenScore = -15;
		else if (ptr->m_GreenValue > params->m_CTCParams[PARAM_CD45AVERAGE10])
			ptr->m_GreenScore = -10;
		else if (ptr->m_GreenValue > params->m_CTCParams[PARAM_CD45AVERAGE5])
			ptr->m_GreenScore = -5;

		ptr->GetBlobData(RED_COLOR)->push_back(rptr);

		for (int k = 0; k < (int)cellCandidates[m]->m_BlueBlobs.size(); k++)
		{
			GetBlobBoundary(cellCandidates[m]->m_BlueBlobs[k]);
			ptr->GetBlobData(BLUE_COLOR)->push_back(cellCandidates[m]->m_BlueBlobs[k]);
			cellCandidates[m]->m_BlueBlobs[k] = NULL;
		}
		FreeBlobList(&cellCandidates[m]->m_BlueBlobs);

		int sum = 0;
		int sumCount = 0;
		if (cellCandidates[m]->m_GreenBlob != NULL)
		{
			CBlobData *greenBlob = cellCandidates[m]->m_GreenBlob;
			GetBlobBoundary(greenBlob);
			ptr->GetBlobData(GREEN_COLOR)->push_back(cellCandidates[m]->m_GreenBlob);
			cellCandidates[m]->m_GreenBlob = NULL;
			memcpy(greenRingMap, greenBlob->m_Map, sizeof(unsigned char) * width * height);
			for (int i = 0; i < params->m_CTCParams[(int)PARAM_RINGPIXELS]; i++)
			{
				ErosionOperation(greenRingMap, temp, width, height);
				memcpy(greenRingMap, temp, sizeof(unsigned char) * width * height);
			}
			CBlobData *blobPtr = new CBlobData(width, height);
			blobPtr->m_Width = rptr->m_Width;
			blobPtr->m_Height = rptr->m_Height;
			GetBlobDataFromMap(blobPtr, greenRingMap, 255, greenImg, 0);
			GetBlobBoundary(blobPtr);
			ptr->GetBlobData(GB_COLORS)->push_back(blobPtr);
			unsigned char *ptrGreenMap = greenBlob->m_Map;
			unsigned char *ptrGreenRingMap = greenRingMap;
			unsigned short *ptrGreenImage = greenImg;
			
			unsigned char label = (unsigned char)255;
			for (int i = 0; i < height; i++)
			{
				for (int j = 0; j < width; j++, ptrGreenMap++, ptrGreenRingMap++, ptrGreenImage++)
				{
					if ((*ptrGreenMap == label) && (*ptrGreenRingMap == (unsigned char)0))
					{
						int greenValue = (int)(*ptrGreenImage);
						sum += greenValue;
						sumCount++;
					}
				}
			}
		}
		
		if (sumCount > 0)
		{
			sum /= sumCount;
			sum -= hitData->GetCPI(GREEN_COLOR);
		}
		else
			sum = 0;

		ptr->m_GreenRingSum = sum;

		if (ptr->m_GreenValue > 0)
			ptr->m_GreenRingValue = (float)(100.0 * ((double)(sum - ptr->m_GreenValue)) / ((double)ptr->m_GreenValue));
		else
			ptr->m_GreenRingValue = 0.0F;

		if (ptr->m_GreenRingValue > params->m_CTCParams[PARAM_CD45RING25])
			ptr->m_GreenRingScore = -25;
		else if (ptr->m_GreenRingValue > params->m_CTCParams[PARAM_CD45RING20])
			ptr->m_GreenRingScore = -20;
		else if (ptr->m_GreenRingValue > params->m_CTCParams[PARAM_CD45RING15])
			ptr->m_GreenRingScore = -15;
		else if (ptr->m_GreenRingValue > params->m_CTCParams[PARAM_CD45RING10])
			ptr->m_GreenRingScore = -10;
		else if (ptr->m_GreenRingValue > params->m_CTCParams[PARAM_CD45RING5])
			ptr->m_GreenRingScore = -5;

		int score = 50 + ptr->m_AspectRatioScore + ptr->m_RedScore + ptr->m_CellSizeScore + ptr->m_NCScore + ptr->m_GreenScore + ptr->m_GreenRingScore;
		ptr->SetScore(score);
		hitData->SetScore(score);
		ptr->SetColorCode(CTC);
		hitsFound->push_back(ptr);
	}

	delete[] greenRingMap;
	delete[] temp;
#endif
	for (int i = 0; i < (int)cellCandidates.size(); i++)
	{
		delete cellCandidates[i];
	}
	cellCandidates.clear();
}

void CHitFindingOperation::GetMaxBlobFromMap(CBlobData *blob)
{
	int width = blob->m_Width;
	int height = blob->m_Height;
	unsigned short *image = new unsigned short[width * height];
	memset(image, 0, sizeof(unsigned short) * width * height);
	unsigned short *map = new unsigned short[width * height];
	memset(map, MAXUINT16, sizeof(unsigned short) * width * height);
	unsigned short *imgPtr = image;
	unsigned short *mapPtr = map;
	unsigned char *blobMapPtr = blob->m_Map;
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++, mapPtr++, imgPtr++, blobMapPtr++)
		{
			if (*blobMapPtr == (unsigned char)255)
			{
				*mapPtr = (unsigned short)0;
				*imgPtr = (unsigned short)255;
			}
		}
	}
	unsigned short lastLabel = 0;
	m_Grouper.LabelConnectedComponents(image, width, height, 255, map, &lastLabel);
	int numLabels = (int)lastLabel;
	int *pixelCounts = new int[numLabels];
	memset(pixelCounts, 0, sizeof(int) * numLabels);
	mapPtr = map;
	for (int k = 0; k < height; k++)
	{
		for (int h = 0; h < width; h++, mapPtr++)
		{
			if ((*mapPtr > 0) && (*mapPtr <= numLabels))
			{
				pixelCounts[(int)*mapPtr - 1]++;
			}
		}
	}
	int maxCount = 0;
	int maxIndex = -1;

	for (int i = 0; i < numLabels; i++)
	{
		if (pixelCounts[i] > maxCount)
		{
			maxCount = pixelCounts[i];
			maxIndex = i;
		}
	}
	int maxIndex1 = maxIndex;
	while (maxCount > m_MaxBlobPixelCount)
	{
		pixelCounts[maxIndex] = 0;
		maxCount = 0;
		maxIndex = -1;

		for (int i = 0; i < numLabels; i++)
		{
			if (pixelCounts[i] > maxCount)
			{
				maxCount = pixelCounts[i];
				maxIndex = i;
			}
		}
	}
	if ((maxIndex == -1) && (maxIndex1 > -1))
		maxIndex = maxIndex1;
	delete[] pixelCounts; 
	mapPtr = map;
	blobMapPtr = blob->m_Map;
	maxIndex++; 
	for (int k = 0; k < height; k++)
	{
		for (int h = 0; h < width; h++, mapPtr++, blobMapPtr++)
		{
			if (*mapPtr == maxIndex)
				*blobMapPtr = (unsigned char)255;
			else
				*blobMapPtr = (unsigned char)0;
		}
	}
	delete[] image;
	delete[] map;
}

void CHitFindingOperation::FreeBlobList(vector<CBlobData *> *blobList)
{
	for (int i = 0; i < (int)blobList->size(); i++)
	{
		if ((*blobList)[i] != NULL)
			delete (*blobList)[i];
	}
	blobList->clear();
}

void CHitFindingOperation::DumpRegionDataToCSVFile(CString filename, vector<CRGNData *> *list, bool isCellMapPlus)
{
	CStdioFile theFile;
	if (theFile.Open(filename, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
	{
		CString textline;
		if (isCellMapPlus)
			textline.Format(_T("HitFinding Results of CellMapPlus %s\n"), CELLMAPPLUS_VERSION);
		else
			textline.Format(_T("HitFinding Results of CRCViewer %s\n"), CELLMAPPLUS_VERSION);
		theFile.WriteString(textline);
		textline.Format(_T("Total Number of Hits = %u\n"), list->size());
		theFile.WriteString(textline);
		textline.Format(_T("RgnIdx,Color,Score,X0,Y0,Left,Top,Right,Bottom,ARatio,RMax,RAvg,GMax,GAvg,BMax,BAvg,RPxls,GPxls,BPxls,RBPxls,LongAxisLength\n"));
		theFile.WriteString(textline);
		for (int i = 0; i < (int)list->size(); i++)
		{
			CRGNData *ptr = (*list)[i];
			int x0, y0;
			ptr->GetPosition(&x0, &y0);
			int left, top, right, bottom;
			ptr->GetBoundingBox(&left, &top, &right, &bottom);
			textline.Format(_T("%d,%u,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n"),
				i + 1, ptr->GetColorCode(), ptr->GetScore(), x0, y0, left, top, right, bottom, ptr->m_AspectRatio,
				ptr->m_RedFrameMax, ptr->m_RedAverage, ptr->m_GreenFrameMax, ptr->m_GreenAverage,
				ptr->m_BlueFrameMax, ptr->m_BlueAverage, ptr->GetPixels(RED_COLOR), ptr->GetPixels(GREEN_COLOR), ptr->GetPixels(BLUE_COLOR),
				ptr->GetPixels(RB_COLORS), ptr->m_LongAxisLength);
			theFile.WriteString(textline);
		}
		theFile.Close();
	}
}

void CHitFindingOperation::GetBlueBlobs(CCTCParams *params, CRGNData *hitData, CBlobData *redBlob)
{
	FreeBlobList(hitData->GetBlobData(BLUE_COLOR));
	int width = redBlob->m_Width;
	int height = redBlob->m_Height;
	unsigned char *newMap1 = new unsigned char[width * height];
	DilationOperation(redBlob->m_Map, newMap1, width, height);
	unsigned short *image = hitData->GetImage(BLUE_COLOR);
	int maxIntensity = 0;
	int minIntensity = MAXUINT16;
	unsigned char *mapPtr = newMap1;
	unsigned short *imgPtr = image;
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++, mapPtr++, imgPtr++)
		{
			if (*mapPtr == (unsigned char)255)
			{
				int intensity = (int)*imgPtr;
				if (intensity > maxIntensity)
					maxIntensity = intensity;
				if (intensity < minIntensity)
					minIntensity = intensity;
			}
		}
	}
	int size = maxIntensity - minIntensity + 1;
	int *histogram = new int[size];
	memset(histogram, 0, sizeof(int) * size);
	unsigned short *map = new unsigned short[width * height];
	memset(map, MAXUINT16, sizeof(unsigned short) * width * height);
	mapPtr = newMap1;
	unsigned short *mapPtr1 = map;
	imgPtr = image;
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++, mapPtr++, imgPtr++, *mapPtr1++)
		{
			if (*mapPtr == (unsigned char)255)
			{
				int intensity = (int)*imgPtr;
				*mapPtr1 = (unsigned short)0;
				histogram[intensity - minIntensity]++;
			}
		}
	}
	delete[] newMap1;
	int threshold = minIntensity + params->m_CTCParams[(int)PARAM_REDTHRESHOLD_DROP];
	unsigned short lastLabel = 0;
	m_Grouper.LabelConnectedComponents(image, width, height, threshold, map, &lastLabel);
	GetFrameMap(map, width, height, (int)lastLabel);
	unsigned short *frameMap = new unsigned short[width * height];
	int previous_count = 0;
	int count = 0;
	vector<PEAKINFO *> peaks;
	for (int i = (size - 1); i > 0; i--)
	{
		count += histogram[i];
		if ((count - previous_count) >= 10)
		{
			previous_count = count;
			memcpy(frameMap, map, sizeof(unsigned short) * width * height);
			lastLabel = 0;
			m_Grouper.LabelConnectedComponents(image, width, height, i, frameMap, &lastLabel);
			GetPeaks(image, frameMap, width, height, (int)lastLabel, DEFAULT_MIN_BLOBPIXELS, i, &peaks);
		}
	}
	delete[] histogram;
	for (int i = 0; i < (int)peaks.size(); i++)
	{
		memcpy(frameMap, map, sizeof(unsigned short) * width * height);
		CBlobData *blob = new CBlobData(width, height);
		int peakThreshold = 5 * peaks[i]->threshold / 8;
		GetBlobDataFromPeak(image, frameMap, width, height, peaks[i], blob, peakThreshold);
		if (blob->m_PixelCount >= DEFAULT_MIN_BLOBPIXELS) 
		{
			hitData->GetBlobData(BLUE_COLOR)->push_back(blob);
		}
		else
			delete blob;
		delete peaks[i];
		peaks[i] = NULL;
	}
	delete[] map;
	delete[] frameMap;
	peaks.clear();
}

void CHitFindingOperation::GetBlobs(CCTCParams *params, CRGNData *hitData, vector<CColorStain *> *stainList)
{
	m_MSERDetector.GetRedBlobs(hitData->GetImage(RED_COLOR), hitData->GetWidth(), hitData->GetHeight(), m_MinBlobPixelCount, m_MaxBlobPixelCount,
		params->m_CTCParams[(int)PARAM_SATURATED_REDCOUNT], 10, hitData->GetBlobData(RED_COLOR));
#if 0 // == 1 for HitViewer Display Red Blob Only
	for (int i=0; i<(int)hitData->GetBlobData(RED_COLOR)->size(); i++)
		GetBlobBoundary((*hitData->GetBlobData(RED_COLOR))[i]);
#else
	if (hitData->GetBlobData(RED_COLOR)->size() > 0)
	{
		GetColorStains(params, hitData, stainList);
	}
#endif
}

void CHitFindingOperation::GetColorStains(CCTCParams *params, CRGNData *hitData, vector<CColorStain *> *stainList)
{
	int width = hitData->GetWidth();
	int height = hitData->GetHeight();
	unsigned short *image = new unsigned short[width * height];
	unsigned short *map = new unsigned short[width * height];

	for (int i = 0; i < (int)hitData->GetBlobData(RED_COLOR)->size(); i++)
	{
		CBlobData *redBlob = (*hitData->GetBlobData(RED_COLOR))[i];
		if ((redBlob->m_PixelCount >= m_MinBlobPixelCount) &&
			(redBlob->m_PixelCount <= m_MaxBlobPixelCount))
		{
			int x0, y0;
			hitData->GetPosition(&x0, &y0);
			CRGNData *ptr = new CRGNData(x0, y0, hitData->GetWidth(), hitData->GetHeight());
			ptr->SetCPI(RED_COLOR, hitData->GetCPI(RED_COLOR));
			ptr->SetCPI(GREEN_COLOR, hitData->GetCPI(GREEN_COLOR));
			ptr->SetCPI(BLUE_COLOR, hitData->GetCPI(BLUE_COLOR));
			
			GetBlueBlobs(params, hitData, redBlob);
			vector<CBlobData *> blueBlobs;
			for (int j = 0; j < (int)(int)hitData->GetBlobData(BLUE_COLOR)->size(); j++)
			{
				CBlobData *blueBlob = (*hitData->GetBlobData(BLUE_COLOR))[j];
				int pixelCount = 0;
				GetIntersectPixels(redBlob, blueBlob, &pixelCount);
				if (pixelCount >= params->m_CTCParams[PARAM_INSIDEREDBLOB_BLUECOUNT])
				{
					blueBlobs.push_back(blueBlob);
					(*hitData->GetBlobData(BLUE_COLOR))[j] = NULL;
				}
			}
			if (blueBlobs.size() > 0)
			{
				unsigned char **ptrs = new unsigned char *[blueBlobs.size() + 1];
				ptrs[0] = redBlob->m_Map;
				for (int k = 0; k < (int)blueBlobs.size(); k++)
				{
					ptrs[k + 1] = blueBlobs[k]->m_Map;
				}
				memset(image, 0, sizeof(unsigned short) * width * height);
				memset(map, 0, sizeof(unsigned short) * width * height);
				for (int k=0; k<height; k++)
				{
					for (int h = 0; h < width; h++)
					{
						bool pixelOn = false;
						for (int l = 0; l <= (int)blueBlobs.size(); l++)
						{
							if ((!pixelOn) && (*ptrs[l] == (unsigned char)255))
								pixelOn = true;
							ptrs[l]++;
						}
						if (pixelOn)
							image[width * k + h] = (unsigned short)255;
					}
				}
				delete[] ptrs;
				unsigned short lastLabel = 0;
				m_Grouper.LabelConnectedComponents(image, width, height, 255, map, &lastLabel);
				int numLabels = (int)lastLabel;
				int *pixelCounts = new int[numLabels];
				memset(pixelCounts, 0, sizeof(int) * numLabels);
				for (int k = 0; k < height; k++)
				{
					for (int h = 0; h < width; h++)
					{
						int mapValue = (int)map[width * k + h];
						if ((mapValue > 0) && (mapValue <= numLabels))
						{
							pixelCounts[mapValue - 1]++;
						}
					}
				}

				int maxCount = 0;
				int maxLabel = 0;
				for (int k = 0; k < numLabels; k++)
				{
					if (pixelCounts[k] > maxCount)
					{
						maxCount = pixelCounts[k];
						maxLabel = k;
					}
				}

				if (maxCount > 0)
				{
					maxLabel++;
					memset(redBlob->m_Map, 0, sizeof(unsigned char) * width * height);
					redBlob->m_PixelCount = 0;
					redBlob->m_Rect.left = width;
					redBlob->m_Rect.right = 0;
					redBlob->m_Rect.top = height;
					redBlob->m_Rect.bottom = 0;
					redBlob->m_MaxIntensity = 0;
					redBlob->m_MaxIntenX = 0;
					redBlob->m_MaxIntenY = 0;
					unsigned short *imgPtr = hitData->GetImage(RED_COLOR);
					unsigned short *mapPtr = map;
					for (int k = 0; k < height; k++)
					{
						for (int h = 0; h < width; h++, mapPtr++, imgPtr++)
						{
							if (maxLabel == (int)*mapPtr)
							{
								redBlob->m_PixelCount++;
								redBlob->m_Map[width * k + h] = (unsigned char)255;
								if (h < redBlob->m_Rect.left)
									redBlob->m_Rect.left = h;
								if (h > redBlob->m_Rect.right)
									redBlob->m_Rect.right = h;
								if (k < redBlob->m_Rect.top)
									redBlob->m_Rect.top = k;
								if (k > redBlob->m_Rect.bottom)
									redBlob->m_Rect.bottom = k;
								if ((int)*imgPtr > redBlob->m_MaxIntensity)
								{
									redBlob->m_MaxIntensity = (int)*imgPtr;
									redBlob->m_MaxIntenX = h;
									redBlob->m_MaxIntenY = k;
								}
							}
						}
					}

					GetBlobBoundary(redBlob);
					CColorStain *stainPtr = new CColorStain();
					stainList->push_back(stainPtr);
					stainPtr->m_RedRgnPtr = ptr;
					stainPtr->m_RedBlob = redBlob;
					(*hitData->GetBlobData(RED_COLOR))[i] = NULL;
					ptr = NULL;
					for (int j = 0; j < (int)blueBlobs.size(); j++)
					{
						stainPtr->m_BlueBlobs.push_back(blueBlobs[j]);
						blueBlobs[j] = NULL;
					}
					stainPtr->m_GreenBlob = new CBlobData(width, height);
					stainPtr->m_GreenBlob->CopyData(redBlob);
				}

				delete[] pixelCounts;
			}
			FreeBlobList(&blueBlobs);
			FreeBlobList(hitData->GetBlobData(BLUE_COLOR));

			if (ptr != NULL)
			{
				delete ptr;
				ptr = NULL;
			}
		}
	}
	FreeBlobList(hitData->GetBlobData(RED_COLOR));
	delete[] image;
	delete[] map;
}

void CHitFindingOperation::GetPeaks(unsigned short *image, unsigned short *map, int width, int height, int numLabels, int minCount, int threshold, vector<PEAKINFO *> *peaks)
{
	int *pixelCounts = new int[numLabels];
	memset(pixelCounts, 0, sizeof(int) * numLabels);
	for (int k = 0; k < height; k++)
	{
		for (int h = 0; h < width; h++)
		{
			int mapValue = (int)map[width * k + h];
			if ((mapValue > 0) && (mapValue <= numLabels))
			{
				pixelCounts[mapValue - 1]++;
			}
		}
	}

	int *blobIndex = new int[numLabels];
	memset(blobIndex, 0, sizeof(int) * numLabels);
	int count = 0;
	vector<PEAKINFO *> peakList;
	for (int i = 0; i < numLabels; i++)
	{
		if (pixelCounts[i] >= minCount)
		{
			count++;
			blobIndex[i] = count;
			PEAKINFO *peak = new PEAKINFO;
			memset(peak, 0, sizeof(PEAKINFO));
			peak->threshold = threshold;
			peakList.push_back(peak);
		}
	}

	unsigned short *imgPtr = image;
	unsigned short *mapPtr = map;
	for (int k = 0; k < height; k++)
	{
		for (int h = 0; h < width; h++, mapPtr++, imgPtr++)
		{
			if ((*mapPtr > 0) && (*mapPtr <= numLabels))
			{
				if (blobIndex[(int)*mapPtr - 1] > 0)
				{
					PEAKINFO *peak = peakList[blobIndex[(int)*mapPtr - 1] - 1];
					peak->pixelcount++;
					if ((int)*imgPtr > peak->maxIntensity)
					{
						peak->maxIntensity = (int)*imgPtr;
						peak->maxIntenX = h;
						peak->maxIntenY = k;
					}
				}
			}
		}
	}

	for (int i = 0; i < count; i++)
	{
		bool found = false;
		for (int j = 0; j < (int)peaks->size(); j++)
		{
			if ((peakList[i]->maxIntensity == (*peaks)[j]->maxIntensity)
				&& (peakList[i]->maxIntenX == (*peaks)[j]->maxIntenX)
				&& (peakList[i]->maxIntenY == (*peaks)[j]->maxIntenY))
			{
				found = true;
				break;
			}
		}
		if (!found)
		{
			peaks->push_back(peakList[i]);
		}
		else
		{
			delete peakList[i];
		}
		peakList[i] = NULL;
	}
	peakList.clear();
	delete[] pixelCounts;
	delete[] blobIndex;
}

void CHitFindingOperation::GetFrameMap(unsigned short *map, int width, int height, int numLabels)
{
	int *pixelCounts = new int[numLabels];
	memset(pixelCounts, 0, sizeof(int) * numLabels);
	for (int k = 0; k < height; k++)
	{
		for (int h = 0; h < width; h++)
		{
			int mapValue = (int)map[width * k + h];
			if ((mapValue > 0) && (mapValue <= numLabels))
			{
				pixelCounts[mapValue - 1]++;
			}
		}
	}

	int *blobIndex = new int[numLabels];
	memset(blobIndex, 0, sizeof(int) * numLabels);
	int count = 0;
	for (int i = 0; i < numLabels; i++)
	{
		if (pixelCounts[i] >= DEFAULT_MIN_BLOBPIXELS)
		{
			count++;
			blobIndex[i] = count;
		}
	}

	unsigned short *mapPtr = map;
	for (int k = 0; k < height; k++)
	{
		for (int h = 0; h < width; h++, mapPtr++)
		{
			if ((*mapPtr > 0) && (*mapPtr <= numLabels))
			{
				if (blobIndex[(int)*mapPtr - 1] > 0)
				{
					*mapPtr = (unsigned short)0;
				}
				else
				{
					*mapPtr = (unsigned short)MAXUINT16;
				}
			}
			else
			{
				*mapPtr = (unsigned short)MAXUINT16;
			}
		}
	}

	delete[] pixelCounts;
	delete[] blobIndex;
}

void CHitFindingOperation::GetBlobDataFromPeak(unsigned short *image, unsigned short *map, int width, int height, PEAKINFO *peak, CBlobData *blob, int threshold)
{
	unsigned short lastLabel = 0;
	m_Grouper.LabelConnectedComponents(image, width, height, threshold, map, &lastLabel);
	int numLabels = (int)lastLabel;
	int *pixelCounts = new int[numLabels];
	memset(pixelCounts, 0, sizeof(int) * numLabels);
	for (int k = 0; k < height; k++)
	{
		for (int h = 0; h < width; h++)
		{
			int mapValue = (int)map[width * k + h];
			if ((mapValue > 0) && (mapValue <= numLabels))
			{
				pixelCounts[mapValue - 1]++;
			}
		}
	}

	int *blobIndex = new int[numLabels];
	memset(blobIndex, 0, sizeof(int) * numLabels);
	vector<CBlobData *> blobList;
	int count = 0;
	for (int i = 0; i < numLabels; i++)
	{
		if (pixelCounts[i] >= DEFAULT_MIN_BLOBPIXELS)
		{
			count++;
			blobIndex[i] = count;
			CBlobData *newBlob = new CBlobData(width, height);
			newBlob->m_Threshold = threshold;
			newBlob->m_Rect.left = width;
			newBlob->m_Rect.top = height;
			blobList.push_back(newBlob);
		}
	}
	
	unsigned short *imgPtr = image;
	unsigned short *mapPtr = map;
	for (int k = 0; k < height; k++)
	{
		for (int h = 0; h < width; h++, mapPtr++, imgPtr++)
		{
			if ((*mapPtr > 0) && (*mapPtr <= numLabels))
			{
				if (blobIndex[(int)*mapPtr - 1] > 0)
				{
					CBlobData *newBlob = blobList[blobIndex[(int)*mapPtr - 1] - 1];
					newBlob->m_PixelCount++;
					newBlob->m_Map[width * k + h] = (unsigned char)255;
					if (h < newBlob->m_Rect.left)
						newBlob->m_Rect.left = h;
					if (h > newBlob->m_Rect.right)
						newBlob->m_Rect.right = h;
					if (k < newBlob->m_Rect.top)
						newBlob->m_Rect.top = k;
					if (k > newBlob->m_Rect.bottom)
						newBlob->m_Rect.bottom = k;
					if ((int)*imgPtr > newBlob->m_MaxIntensity)
					{
						newBlob->m_MaxIntensity = (int)*imgPtr;
						newBlob->m_MaxIntenX = h;
						newBlob->m_MaxIntenY = k;
					}
				}
			}
		}
	}

	delete[] pixelCounts;
	delete[] blobIndex;

	for (int i = 0; i < (int)blobList.size(); i++)
	{
		if ((blobList[i]->m_MaxIntensity == peak->maxIntensity) &&
			(blobList[i]->m_MaxIntenX == peak->maxIntenX) &&
			(blobList[i]->m_MaxIntenY == peak->maxIntenY))
		{
			blob->CopyData(blobList[i]);
			break;
		}
	}
	FreeBlobList(&blobList);
}

void CHitFindingOperation::SetPixelCountLimits(int minPixels, int maxPixels)
{
	m_MinBlobPixelCount = minPixels;
	m_MaxBlobPixelCount = maxPixels;
}

void CHitFindingOperation::FillCellRegionList(vector<CRGNData *> *rgnList, CListCtrl *cellRegionList,
	int sortColumn, int *FoundHitCount)
{
	if (rgnList->size() > 0)
	{
		int size = (int)rgnList->size();
		int *valueArray = new int[size];
		int *idxArray = new int[size];
		for (int i = 0; i < size; i++)
			idxArray[i] = i;
		switch (sortColumn)
		{
		case 0:
			for (int i = 0; i < size; i++)
			{
				valueArray[i] = i;
			}
			break;
		case 1:
			for (int i = 0; i < size; i++)
			{
				valueArray[i] = (*rgnList)[i]->GetScore();
			}
			break;
		case 2:
			for (int i = 0; i < size; i++)
			{
				int x0, y0;
				(*rgnList)[i]->GetPosition(&x0, &y0);
				valueArray[i] = x0;
			}
			break;
		case 3:
			for (int i = 0; i < size; i++)
			{
				int x0, y0;
				(*rgnList)[i]->GetPosition(&x0, &y0);
				valueArray[i] = y0;
			}
			break;
		default:
			for (int i = 0; i < size; i++)
			{
				valueArray[i] = i;
			}
			break;
		}

		if ((sortColumn == 1) || (sortColumn == 2) || (sortColumn == 3))
		{
			int temp = 0;
			for (int i = 0; i < (size - 1); i++)
			{
				for (int j = i + 1; j < size; j++)
				{
					if (valueArray[j] > valueArray[i])
					{
						temp = idxArray[i];
						idxArray[i] = idxArray[j];
						idxArray[j] = temp;
						temp = valueArray[i];
						valueArray[i] = valueArray[j];
						valueArray[j] = temp;
					}
				}
			}
		}

		int itemIndex = 0;
		for (int i = 0; i < (int)rgnList->size(); i++)
		{
			CString RegionIndex;
			if ((*rgnList)[idxArray[i]]->m_HitIndex > 0)
				RegionIndex.Format(_T("%d"), (*rgnList)[idxArray[i]]->m_HitIndex);
			else
				RegionIndex.Format(_T("%d"), (idxArray[i] + 1));
			CString CTCScore;
			CTCScore.Format(_T("%d"), (*rgnList)[idxArray[i]]->GetScore());
			int x0, y0;
			(*rgnList)[idxArray[i]]->GetPosition(&x0, &y0);
			CString X0Pos;
			X0Pos.Format(_T("%d"), x0);
			CString Y0Pos;
			Y0Pos.Format(_T("%d"), y0);
			int idx = cellRegionList->InsertItem(itemIndex++, RegionIndex);
			int subItemIndex = 1;
			cellRegionList->SetItemText(idx, subItemIndex++, CTCScore);
			cellRegionList->SetItemText(idx, subItemIndex++, X0Pos);
			cellRegionList->SetItemText(idx, subItemIndex++, Y0Pos);
			if ((*rgnList)[idxArray[i]]->GetScore() > 0)
				(*FoundHitCount)++;
		}

		delete[] idxArray;
		delete[] valueArray;
	}
}

void CHitFindingOperation::FillCellScoreList(CRGNData *hitData, CListCtrl *cellScoreList, int hitIndex)
{
	cellScoreList->DeleteAllItems();
	CString labelStr;
	int itemIndex = 0;
	labelStr = _T("RgnIdx");
	int idx = cellScoreList->InsertItem(itemIndex++, labelStr);
	labelStr.Format(_T("%d"), hitIndex);
	cellScoreList->SetItemText(idx, 1, labelStr);
	labelStr = _T("Base");
	idx = cellScoreList->InsertItem(itemIndex++, labelStr);
	labelStr = _T("50");
	cellScoreList->SetItemText(idx, 2, labelStr);
	labelStr = _T("ARatio");
	idx = cellScoreList->InsertItem(itemIndex++, labelStr);
	labelStr.Format(_T("%d"), hitData->m_AspectRatio);
	cellScoreList->SetItemText(idx, 1, labelStr);
	labelStr.Format(_T("%d"), hitData->m_AspectRatioScore);
	cellScoreList->SetItemText(idx, 2, labelStr);
	labelStr = _T("RedAvg");
	idx = cellScoreList->InsertItem(itemIndex++, labelStr);
	labelStr.Format(_T("%d"), hitData->m_RedValue);
	cellScoreList->SetItemText(idx, 1, labelStr);
	labelStr.Format(_T("%d"), hitData->m_RedScore);
	cellScoreList->SetItemText(idx, 2, labelStr);
	labelStr = _T("CellSize");
	idx = cellScoreList->InsertItem(itemIndex++, labelStr);
	labelStr.Format(_T("%.1f"), hitData->m_CellSizeValue);
	cellScoreList->SetItemText(idx, 1, labelStr);
	labelStr.Format(_T("%d"), hitData->m_CellSizeScore);
	cellScoreList->SetItemText(idx, 2, labelStr);
	labelStr = _T("Blue Count");
	idx = cellScoreList->InsertItem(itemIndex++, labelStr);
	labelStr.Format(_T("%d"), hitData->GetPixels(RB_COLORS));
	cellScoreList->SetItemText(idx, 1, labelStr);
	labelStr = _T("Red Count");
	idx = cellScoreList->InsertItem(itemIndex++, labelStr);
	labelStr.Format(_T("%d"), hitData->GetPixels(RED_COLOR));
	cellScoreList->SetItemText(idx, 1, labelStr);
	labelStr = _T("N/C Ratio");
	idx = cellScoreList->InsertItem(itemIndex++, labelStr);
	labelStr.Format(_T("%.1f"), hitData->m_NCRatio);
	cellScoreList->SetItemText(idx, 1, labelStr);
	labelStr.Format(_T("%d"), hitData->m_NCScore);
	cellScoreList->SetItemText(idx, 2, labelStr);
	labelStr = _T("GreenAvg");
	idx = cellScoreList->InsertItem(itemIndex++, labelStr);
	labelStr.Format(_T("%d"), hitData->m_GreenValue);
	cellScoreList->SetItemText(idx, 1, labelStr);
	labelStr.Format(_T("%d"), hitData->m_GreenScore);
	cellScoreList->SetItemText(idx, 2, labelStr);
	labelStr = _T("Ring");
	idx = cellScoreList->InsertItem(itemIndex++, labelStr);
	labelStr.Format(_T("%d"), hitData->m_GreenRingSum);
	cellScoreList->SetItemText(idx, 1, labelStr);
	labelStr = _T("%Brighter");
	idx = cellScoreList->InsertItem(itemIndex++, labelStr);
	labelStr.Format(_T("%.1f"), hitData->m_GreenRingValue);
	cellScoreList->SetItemText(idx, 1, labelStr);
	labelStr.Format(_T("%d"), hitData->m_GreenRingScore);
	cellScoreList->SetItemText(idx, 2, labelStr);
	labelStr = _T("Total");
	idx = cellScoreList->InsertItem(itemIndex++, labelStr);
	labelStr.Format(_T("%d"), hitData->GetScore());
	cellScoreList->SetItemText(idx, 2, labelStr);
	labelStr = _T("Green Count");
	idx = cellScoreList->InsertItem(itemIndex++, labelStr);
	labelStr.Format(_T("%d"), hitData->GetPixels(GREEN_COLOR));
	cellScoreList->SetItemText(idx, 1, labelStr);
	labelStr = _T("G/R Ratio");
	idx = cellScoreList->InsertItem(itemIndex++, labelStr);
	if (hitData->GetPixels(RED_COLOR) > 0)
	{
		float ratio = (float)(100.0 * hitData->GetPixels(GREEN_COLOR) / hitData->GetPixels(RED_COLOR));
		labelStr.Format(_T("%.1f"), ratio);
		cellScoreList->SetItemText(idx, 1, labelStr);
	}
	if (hitData->GetScoreIndex() > 0)
	{
		labelStr = _T("Score Rank");
		idx = cellScoreList->InsertItem(itemIndex++, labelStr);
		labelStr.Format(_T("%d"), hitData->GetScoreIndex());
		cellScoreList->SetItemText(idx, 1, labelStr);
	}
	labelStr = _T("Length");
	idx = cellScoreList->InsertItem(itemIndex++, labelStr);
	labelStr.Format(_T("%d"), hitData->m_LongAxisLength);
	cellScoreList->SetItemText(idx, 1, labelStr);
}

void CHitFindingOperation::GetCenterCell(CRGNData *ptr, vector<CRGNData *> *cellList,
	int x, int y, CListCtrl *cellScoreList, int hitIndex)
{
	if (cellList->size() > 0)
	{
		FreeBlobList(ptr->GetBlobData(RED_COLOR));
		FreeBlobList(ptr->GetBlobData(GREEN_COLOR));
		FreeBlobList(ptr->GetBlobData(BLUE_COLOR));
		FreeBlobList(ptr->GetBlobData(RB_COLORS));
		FreeBlobList(ptr->GetBlobData(GB_COLORS));
	}
	if ((x != -1) && (y != -1))
	{
		int index = -1;
		for (int i = 0; i < (int)cellList->size(); i++)
		{
			int left, right, top, bottom;
			(*cellList)[i]->GetBoundingBox(&left, &top, &right, &bottom);
			if ((x >= left) && (x <= right) && (y >= top) && (y <= bottom))
			{
				index = i;
				break;
			}
		}

		if (index >= 0)
		{
			ptr->CopyRegionData((*cellList)[index]);
			ptr->m_HitIndex = hitIndex;
			if (cellScoreList != NULL)
				FillCellScoreList(ptr, cellScoreList, hitIndex);
		}
	}
	else
	{
		if (cellList->size() > 0)
		{
			int index = -1;
			int minDistance = MAXUINT16;
			int centerX = ptr->GetWidth() / 2;
			int centerY = ptr->GetHeight() / 2;
			for (int i = 0; i < (int)cellList->size(); i++)
			{
				int left, right, top, bottom;
				(*cellList)[i]->GetBoundingBox(&left, &top, &right, &bottom);
				int dx = centerX - (left + right) / 2;
				int dy = centerY - (top + bottom) / 2;
				int distance = dx * dx + dy * dy;
				if (distance < minDistance)
				{
					minDistance = distance;
					index = i;
				}
			}
			if (index > -1)
			{
				ptr->CopyRegionData((*cellList)[index]);
				ptr->m_HitIndex = hitIndex;
				if (cellScoreList != NULL)
					FillCellScoreList(ptr, cellScoreList, hitIndex);
			}
		}
	}
}

void CHitFindingOperation::ScanImageForConfirmedCTCs(CCTCParams *params, CSingleChannelTIFFData *redImage, CSingleChannelTIFFData *greenImage,
	CSingleChannelTIFFData *blueImage, CRGNData *data, vector<CRGNData *> *outputList)
{
	int imageWidth = redImage->GetImageWidth();
	int imageHeight = redImage->GetImageHeight();
	int regionWidth = data->GetWidth();
	int regionHeight = data->GetHeight();
	int sampleWidth = regionWidth / 2;
	int sampleHeight = regionHeight / 2;
	int x0, y0;
	data->GetPosition(&x0, &y0);
	int rowIndex0 = (y0 / sampleHeight);
	int columnIndex0 = (x0 / sampleWidth);
	unsigned short *redRegionImage = new unsigned short[regionWidth * regionHeight];
	unsigned short *greenRegionImage = new unsigned short[regionWidth * regionHeight];
	unsigned short *blueRegionImage = new unsigned short[regionWidth * regionHeight];

	CString message;
	message.Format(_T("x0=%d,y0=%d,x1=%d,y1=%d"), x0, y0, x0+regionWidth, y0+regionHeight);
	m_Log->Message(message);
	vector<CRGNData *> cellList;
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			int x1 = (columnIndex0 + j) * sampleWidth;
			int y1 = (rowIndex0 + i) * sampleHeight;
			bool retRed = redImage->GetImageRegion(x1, y1, regionWidth, regionHeight, redRegionImage);
			bool retGreen = greenImage->GetImageRegion(x1, y1, regionWidth, regionHeight, greenRegionImage);
			bool retBlue = blueImage->GetImageRegion(x1, y1, regionWidth, regionHeight, blueRegionImage);
			if (retRed && retGreen && retBlue)
			{
				CRGNData *region = new CRGNData(x1, y1, regionWidth, regionHeight);
				region->SetCutoff(RED_COLOR, params->m_CTCParams[(int)PARAM_RED_CUTOFF]);
				region->SetCutoff(GREEN_COLOR, params->m_CTCParams[(int)PARAM_GREEN_CUTOFF]);
				region->SetCutoff(BLUE_COLOR, params->m_CTCParams[(int)PARAM_BLUE_CUTOFF]);
				region->SetContrast(RED_COLOR, params->m_CTCParams[(int)PARAM_RED_CONTRAST]);
				region->SetContrast(GREEN_COLOR, params->m_CTCParams[(int)PARAM_GREEN_CONTRAST]);
				region->SetContrast(BLUE_COLOR, params->m_CTCParams[(int)PARAM_BLUE_CONTRAST]);
				region->SetThreshold(RED_COLOR, params->m_CTCParams[(int)PARAM_RED_THRESHOLD]);
				region->SetThreshold(GREEN_COLOR, params->m_CTCParams[(int)PARAM_GREEN_THRESHOLD]);
				region->SetThreshold(BLUE_COLOR, params->m_CTCParams[(int)PARAM_BLUE_THRESHOLD]);
				region->SetCPI(RED_COLOR, redImage->GetCPI());
				region->SetCPI(GREEN_COLOR, greenImage->GetCPI());
				region->SetCPI(BLUE_COLOR, blueImage->GetCPI());
				region->m_HitIndex = data->m_HitIndex;
				region->SetImages(redRegionImage, greenRegionImage, blueRegionImage);
				ProcessOneROI(params, region, &cellList);
				if ((int)cellList.size() > 0)
				{
					message.Format(_T("i=%d,j=%d,x=%d,y=%d,region#=%d"), i, j, sampleWidth*(columnIndex0 + j), sampleHeight*(rowIndex0 + i), cellList.size());
					m_Log->Message(message);
					for (int k = 0; k < (int)cellList.size(); k++)
					{
						message.Format(_T("k=%d,pixels=%d,redInten=%d,x=%d,y=%d,intensityRange=%d"), k, cellList[k]->GetPixels(RED_COLOR), (*cellList[k]->GetBlobData(RED_COLOR))[0]->m_MaxIntensity,
							(*cellList[k]->GetBlobData(RED_COLOR))[0]->m_MaxIntenX + sampleWidth*(columnIndex0 + j), (*cellList[k]->GetBlobData(RED_COLOR))[0]->m_MaxIntenY + sampleHeight*(rowIndex0 + i),
							(*cellList[k]->GetBlobData(RED_COLOR))[0]->m_IntensityRange);
						m_Log->Message(message);
					}
				}
				region->NullImages();
				delete region;
				if (cellList.size() > 0)
				{
					for (int k = 0; k < (int)cellList.size(); k++)
					{
						if (AlreadyFound(cellList[k], outputList))
						{
							message.Format(_T("k=%d, Already"), k);
							m_Log->Message(message);
							delete cellList[k];
							cellList[k] = NULL;
						}
						else
						{
							if (!IsAQualifiedCell(cellList[k]))
							{
								message.Format(_T("k=%d, Not Qualified"), k);
								m_Log->Message(message);
								delete cellList[k];
								cellList[k] = NULL;
							}
							else if (IsInGivenFrame(cellList[k], x0, y0, regionWidth, regionHeight))
							{
								message.Format(_T("Keep: k=%d"), k);
								m_Log->Message(message);
								outputList->push_back(cellList[k]);
								cellList[k] = NULL;
							}
						}
					}
					FreeRGNList(&cellList);
				}
			}
		}
	}
	
	delete[] redRegionImage;
	delete[] greenRegionImage;
	delete[] blueRegionImage;
}

bool CHitFindingOperation::AlreadyFound(CRGNData *cell, vector<CRGNData *> *cellList)
{
	bool ret = false;
	int x0, y0;
	cell->GetPosition(&x0, &y0);
	CBlobData *blob = (*cell->GetBlobData(RED_COLOR))[0];
	int maxInten = blob->m_MaxIntensity;
	int maxj = blob->m_MaxIntenX + x0;
	int maxi = blob->m_MaxIntenY + y0;
	for (int i = 0; i < (int)cellList->size(); i++)
	{
		int x1, y1;
		CRGNData *region = (*cellList)[i];
		region->GetPosition(&x1, &y1);
		int left0, top0, right0, bottom0;
		int left1, top1, right1, bottom1;
		region->GetBoundingBox(&left0, &top0, &right0, &bottom0);
		left1 = left0 + x1;
		right1 = right0 + x1;
		top1 = top0 + y1;
		bottom1 = bottom0 + y1;
		if ((maxj >= left1) && (maxj <= right1) && (maxi >= top1) && (maxi <= bottom1))
		{
			CBlobData *blob1 = (*region->GetBlobData(RED_COLOR))[0];
			int maxIntenX = blob1->m_MaxIntenX + x1;
			int maxIntenY = blob1->m_MaxIntenY + y1;
			if ((blob1->m_MaxIntensity == maxInten) &&
				(abs(maxIntenX - maxj) < 5) &&
				(abs(maxIntenY - maxi) < 5))
			{
				if ((left0 == 0) || (top0 == 0) || (right0 == (region->GetWidth() - 1)) || (bottom0 == (region->GetHeight() - 1)))
				{
					region->CopyRegionData(cell);
				}
				ret = true;
				break;
			}
		}
	}
	return ret;
}

CRGNData *CHitFindingOperation::GetTheSameCell(int maxInten, int maxIntenX, int maxIntenY, vector<CRGNData *> *cellList)
{
	CRGNData *newCell = NULL;

	for (int i = 0; i < (int)cellList->size(); i++)
	{
		int x0, y0;
		CRGNData *region = (*cellList)[i];
		region->GetPosition(&x0, &y0);
		int left, top, right, bottom;
		region->GetBoundingBox(&left, &top, &right, &bottom);
		left += x0;
		right += x0;
		top += y0;
		bottom += y0;
		if ((maxIntenX >= left) && (maxIntenY >= top) && (maxIntenX <= right) && (maxIntenY <= bottom))
		{
			CBlobData *blob = (*region->GetBlobData(RED_COLOR))[0];
			int maxj = blob->m_MaxIntenX + x0;
			int maxi = blob->m_MaxIntenY + y0;
			if ((maxInten == blob->m_MaxIntensity) &&
				(abs(maxIntenX - maxj) < 5) &&
				(abs(maxIntenY - maxi) < 5))
			{
				newCell = (*cellList)[i];
				(*cellList)[i] = NULL;
			}
		}
	}
	return newCell;
}

bool CHitFindingOperation::IsInGivenFrame(CRGNData *newCell, int x0, int y0, int width, int height)
{
	bool ret = false;
	int x1, y1;
	newCell->GetPosition(&x1, &y1);
	CBlobData *blob = (*newCell->GetBlobData(RED_COLOR))[0];
	int maxj = blob->m_MaxIntenX + x1;
	int maxi = blob->m_MaxIntenY + y1;
	if ((maxj >= x0) && (maxj < (x0 + width)) && (maxi >= y0) && (maxi <= (y0 + height)))
	{
		ret = true;
	}
	return ret;
}

void CHitFindingOperation::FreeRGNList(vector<CRGNData *> *rgnList)
{
	for (int i = 0; i < (int)rgnList->size(); i++)
	{
		if ((*rgnList)[i] != NULL)
		{
			delete (*rgnList)[i];
			(*rgnList)[i] = NULL;
		}
	}
	(*rgnList).clear();
}

void CHitFindingOperation::SortData(vector<CRGNData *> *list)
{
	CRGNData *temp;

	for (int i = 0; i < (int)(list->size() - 1); i++)
	{
		for (int j = i + 1; j < (int)list->size(); j++)
		{
			if ((*list)[i]->GetScore() < (*list)[j]->GetScore())
			{
				temp = (*list)[i];
				(*list)[i] = (*list)[j];
				(*list)[j] = temp;
			}
		}
	}
}

void CHitFindingOperation::ScanImage(CCTCParams *params, CSingleChannelTIFFData *redImage, CSingleChannelTIFFData *greenImage,
	CSingleChannelTIFFData *blueImage, vector<CRGNData *> *outputList, int *confidence)
{
	int regionWidth = SAMPLEFRAMEWIDTH;
	int regionHeight = SAMPLEFRAMEHEIGHT;

	int sampleWidth = regionWidth / 2;
	int sampleHeight = regionHeight / 2;

	int NumColumns = (redImage->GetImageWidth() / sampleWidth) - 1;
	int NumRows = (redImage->GetImageHeight() / sampleHeight) - 1;

	unsigned short *redRegionImage = new unsigned short[regionWidth * regionHeight];
	unsigned short *greenRegionImage = new unsigned short[regionWidth * regionHeight];
	unsigned short *blueRegionImage = new unsigned short[regionWidth * regionHeight];

	if (m_Log != NULL)
	{
		CString message;
		message.Format(_T("NumColumns=%d, NumRows=%d"), NumColumns, NumRows);
		m_Log->Message(message);
	}

	vector<CRGNData *> cellList;
	vector<CRGNData *> resultList;
	for (int i = 0; i < NumRows; i++)
	{
		for (int j = 0; j < NumColumns; j++)
		{
			int x0 = sampleWidth * j;
			int y0 = sampleHeight * i; 
			bool retRed = redImage->GetImageRegion(x0, y0, regionWidth, regionHeight, redRegionImage);
			bool retGreen = greenImage->GetImageRegion(x0, y0, regionWidth, regionHeight, greenRegionImage);
			bool retBlue = blueImage->GetImageRegion(x0, y0, regionWidth, regionHeight, blueRegionImage);
			if (retRed && retGreen && retBlue)
			{
				CRGNData *region = new CRGNData(x0, y0, regionWidth, regionHeight);
				region->SetCutoff(RED_COLOR, params->m_CTCParams[(int)PARAM_RED_CUTOFF]);
				region->SetCutoff(GREEN_COLOR, params->m_CTCParams[(int)PARAM_GREEN_CUTOFF]);
				region->SetCutoff(BLUE_COLOR, params->m_CTCParams[(int)PARAM_BLUE_CUTOFF]);
				region->SetContrast(RED_COLOR, params->m_CTCParams[(int)PARAM_RED_CONTRAST]);
				region->SetContrast(GREEN_COLOR, params->m_CTCParams[(int)PARAM_GREEN_CONTRAST]);
				region->SetContrast(BLUE_COLOR, params->m_CTCParams[(int)PARAM_BLUE_CONTRAST]);
				region->SetThreshold(RED_COLOR, params->m_CTCParams[(int)PARAM_RED_THRESHOLD]);
				region->SetThreshold(GREEN_COLOR, params->m_CTCParams[(int)PARAM_GREEN_THRESHOLD]);
				region->SetThreshold(BLUE_COLOR, params->m_CTCParams[(int)PARAM_BLUE_THRESHOLD]);
				region->SetCPI(RED_COLOR, redImage->GetCPI());
				region->SetCPI(GREEN_COLOR, greenImage->GetCPI());
				region->SetCPI(BLUE_COLOR, blueImage->GetCPI());
				region->SetImages(redRegionImage, greenRegionImage, blueRegionImage);
				ProcessOneROI(params, region, &cellList);
				region->NullImages();
				delete region;
				if (cellList.size() > 0)
				{
					for (int k = 0; k < (int)cellList.size(); k++)
					{
						if (AlreadyFound(cellList[k], &resultList))
						{
							delete cellList[k];
							cellList[k] = NULL;
						}
						else if (!IsAQualifiedCell(cellList[k]))
						{
							delete cellList[k];
							cellList[k] = NULL;
						}
						else
						{
							resultList.push_back(cellList[k]);
							cellList[k] = NULL;
						}
					}
					cellList.clear();
				}
			}
		}
		{
			CString message;
			message.Format(_T("Row#=%d,TotalHitsSoFar=%u"), i, resultList.size());
			m_Log->Message(message);
		}
	}
	delete[] redRegionImage;
	delete[] greenRegionImage;
	delete[] blueRegionImage;
	MoveToCenter(&resultList, outputList);
	FreeRGNList(&resultList);
	SortData(outputList);
	int highIntensityRangeCount = 0;
	for (int i = 0; i < (int)outputList->size(); i++)
	{
		CRGNData *region = (*outputList)[i];
		CBlobData *blob = (*region->GetBlobData(RED_COLOR))[0];
		if (blob->m_IntensityRange > 250)
			highIntensityRangeCount++;
	}
	if (outputList->size() > 0)
	{
		*confidence = 100 * highIntensityRangeCount / (int)outputList->size();
	}
	else
		*confidence = 0;
	{
		CString message;
		message.Format(_T("highIntensityRangeCount=%d, Confidence=%d"), highIntensityRangeCount, *confidence);
		m_Log->Message(message);
	}
}

void CHitFindingOperation::MoveToCenter(vector<CRGNData *> *inputList, vector<CRGNData *> *outputList)
{
	for (int i = 0; i < (int)inputList->size(); i++)
	{
		CRGNData *ptr = (*inputList)[i];
		int left, right, top, bottom;
		ptr->GetBoundingBox(&left, &top, &right, &bottom);
		(*inputList)[i] = NULL;
		if ((left == 0) || (top == 0) || (right == (SAMPLEFRAMEWIDTH - 1)) || (bottom == (SAMPLEFRAMEHEIGHT - 1)))
		{
			delete ptr;
		}
		else
		{
			int centerX = (left + right) / 2;
			int centerY = (top + bottom) / 2;
			int xShift = (SAMPLEFRAMEWIDTH / 2) - centerX;
			int yShift = (SAMPLEFRAMEHEIGHT / 2) - centerY;
			int x0, y0;
			ptr->GetPosition(&x0, &y0);
			x0 -= xShift;
			y0 -= yShift;
			ptr->SetPosition(x0, y0);
			ptr->SetBoundingBox(left + xShift, top + yShift, right + xShift, bottom + yShift);
			CBlobData *blob = NULL;
			vector<CBlobData *> *blobList = ptr->GetBlobData(RED_COLOR);
			if (blobList->size() > 0)
			{
				blob = (*blobList)[0];
				if (blob != NULL)
				{
					for (int j = 0; j < (int)blob->m_Boundary->size(); j++)
					{
						int x = GetX((*blob->m_Boundary)[j]);
						int y = GetY((*blob->m_Boundary)[j]);
						(*blob->m_Boundary)[j] = GetPos((unsigned short)(x + xShift), (unsigned short)(y + yShift));
					}
					blob->m_MaxIntenX += xShift;
					blob->m_MaxIntenY += yShift;
				}
			}
			blobList = ptr->GetBlobData(BLUE_COLOR);
			if (blobList->size() > 0)
			{
				blob = (*blobList)[0];
				if (blob != NULL)
				{
					for (int j = 0; j < (int)blob->m_Boundary->size(); j++)
					{
						int x = GetX((*blob->m_Boundary)[j]);
						int y = GetY((*blob->m_Boundary)[j]);
						(*blob->m_Boundary)[j] = GetPos((unsigned short)(x + xShift), (unsigned short)(y + yShift));
					}
				}
			}
			blobList = ptr->GetBlobData(GREEN_COLOR);
			if (blobList->size() > 0)
			{
				blob = (*blobList)[0];
				if (blob != NULL)
				{
					for (int j = 0; j < (int)blob->m_Boundary->size(); j++)
					{
						int x = GetX((*blob->m_Boundary)[j]);
						int y = GetY((*blob->m_Boundary)[j]);
						(*blob->m_Boundary)[j] = GetPos((unsigned short)(x + xShift), (unsigned short)(y + yShift));
					}
				}
			}
			blobList = ptr->GetBlobData(GB_COLORS);
			if (blobList->size() > 0)
			{
				blob = (*blobList)[0];
				if (blob != NULL)
				{
					for (int j = 0; j < (int)blob->m_Boundary->size(); j++)
					{
						int x = GetX((*blob->m_Boundary)[j]);
						int y = GetY((*blob->m_Boundary)[j]);
						(*blob->m_Boundary)[j] = GetPos((unsigned short)(x + xShift), (unsigned short)(y + yShift));
					}
				}
			}

			outputList->push_back(ptr);
		}
	}
}

bool CHitFindingOperation::IsAQualifiedCell(CRGNData *cell)
{
	bool ret = false;
#if 0
	if (cell->m_AspectRatio < 30)
		return ret;

	if ((cell->m_LongAxisLength < 10) ||
		(cell->m_LongAxisLength > 61))
		return ret;
#endif
	if (cell->m_GreenValue >= 300)
		return ret;

	ret = true;
	return ret;
}

void CHitFindingOperation::GetIntersectPixels(CBlobData *blob1, CBlobData *blob2, int *pixelCount)
{
	int count = 0;
	for (int i = 0; i < blob1->m_Height; i++)
	{
		for (int j = 0; j < blob1->m_Width; j++)
		{
			if ((blob2->m_Map[blob1->m_Width * i + j] == (unsigned char)255) &&
				(NeighborhoodON(blob1, i, j))) 
				count++;
		}
	}
	*pixelCount = count;
}

void CHitFindingOperation::ErosionOperation(unsigned char *inImage, unsigned char *outImage, int width, int height)
{
	memset(outImage, 0, sizeof(unsigned char) * width * height);

	for (int i = 1; i < (height - 1); i++)
	{
		for (int j = 1; j < (width - 1); j++)
		{
			bool keep = true;
			for (int k = -1; k < 2; k++)
			{
				for (int h = -1; h < 2; h++)
				{
					if (inImage[width * (i + k) + (j + h)] != (unsigned char)255)
					{
						keep = false;
						break;
					}
				}
				if (!keep)
					break;
			}
			if (keep)
				outImage[width * i + j] = (unsigned char)255;
		}
	}
}

void CHitFindingOperation::DilationOperation(unsigned char *inImage, unsigned char *outImage, int width, int height)
{
	memset(outImage, 0, sizeof(unsigned char) * width * height);

	for (int i = 1; i < (height - 1); i++)
	{
		for (int j = 1; j < (width - 1); j++)
		{
			bool keep = false;
			for (int k = -1; k < 2; k++)
			{
				for (int h = -1; h < 2; h++)
				{
					if (inImage[width * (i + k) + (j + h)] == (unsigned char)255)
					{
						keep = true;
						break;
					}
				}
				if (keep)
					break;
			}
			if (keep)
				outImage[width * i + j] = (unsigned char)255;
		}
	}
}

void CHitFindingOperation::GetBlobDataFromMap(CBlobData *blob, unsigned char *map, int label, unsigned short *image, int threshold)
{
	blob->m_Rect.left = blob->m_Width;
	blob->m_Rect.right = 0;
	blob->m_Rect.top = blob->m_Height;
	blob->m_Rect.bottom = 0;
	blob->m_MaxIntensity = 0;
	blob->m_MaxIntenX = 0;
	blob->m_MaxIntenY = 0;
	blob->m_PixelCount = 0;
	blob->m_Threshold = threshold;
	unsigned char *mapPtr = map;
	unsigned char *temp = new unsigned char[blob->m_Width * blob->m_Height];
	unsigned char *tempPtr = temp;
	unsigned char blobLabel = (unsigned char)label;
	unsigned short *imgPtr = image;
	for (int i = 0; i < blob->m_Height; i++)
	{
		for (int j = 0; j < blob->m_Width; j++, mapPtr++, tempPtr++, imgPtr++)
		{
			if (*mapPtr == blobLabel)
			{
				if (i < blob->m_Rect.top)
					blob->m_Rect.top = i;
				if (i > blob->m_Rect.bottom)
					blob->m_Rect.bottom = i;
				if (j < blob->m_Rect.left)
					blob->m_Rect.left = j;
				if (j > blob->m_Rect.right)
					blob->m_Rect.right = j;
				*tempPtr = (unsigned char)255;
				blob->m_PixelCount++;
				if (*imgPtr > blob->m_MaxIntensity)
				{
					blob->m_MaxIntensity = *imgPtr;
					blob->m_MaxIntenX = j;
					blob->m_MaxIntenY = i;
				}
			}
			else
				*tempPtr = (unsigned char)0;
		}
	}
	memcpy(blob->m_Map, temp, sizeof(unsigned char) * blob->m_Width * blob->m_Height);
	delete[] temp;
}

void CHitFindingOperation::GetBlobBoundary(CBlobData *blob)
{
	unsigned char *temp1 = new unsigned char[blob->m_Width * blob->m_Height];
	ErosionOperation(blob->m_Map, temp1, blob->m_Width, blob->m_Height);
	blob->m_Boundary->clear();
	for (int i = 0; i < blob->m_Height; i++)
	{
		for (int j = 0; j < blob->m_Width; j++)
		{
			if ((blob->m_Map[blob->m_Width * i + j] == (unsigned char)255) &&
				(temp1[blob->m_Width * i + j] == (unsigned char)0))
			{
				unsigned int pos = GetPos((unsigned short)j, (unsigned short)i);
				blob->m_Boundary->push_back(pos);
			}
		}
	}
	delete[] temp1;
}

void CHitFindingOperation::SaveImage(CString filename, unsigned short *map, int width, int height)
{
	CStdioFile theFile;
	if (theFile.Open(filename, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
	{
		CString textline;
		for (int i = 0; i < height; i++)
		{
			textline = _T("");
			for (int j = 0; j < width; j++)
			{
				CString textword;
				if (j < (width - 1))
					textword.Format(_T("%d,"), map[width * i + j]);
				else
					textword.Format(_T("%d\n"), map[width * i + j]);
				textline += textword;
			}
			theFile.WriteString(textline);
		}
		theFile.Close();
	}
}

void CHitFindingOperation::SaveMap(CString filename, unsigned char *map, int width, int height)
{
	CStdioFile theFile;
	if (theFile.Open(filename, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
	{
		CString textline;
		for (int i = 0; i < height; i++)
		{
			textline = _T("");
			for (int j = 0; j < width; j++)
			{
				CString textword;
				if (j < (width - 1))
					textword.Format(_T("%d,"), map[width * i + j]);
				else
					textword.Format(_T("%d\n"), map[width * i + j]);
				textline += textword;
			}
			theFile.WriteString(textline);
		}
		theFile.Close();
	}
}

unsigned int CHitFindingOperation::GetPos(unsigned short x, unsigned short y)
{
	unsigned int pos = (y & 0xFFFF) << 16;
	pos |= (x & 0xFFFF);

	return pos;
}

int CHitFindingOperation::GetX(unsigned int pos)
{
	return (int)(pos & 0xFFFF);
}

int CHitFindingOperation::GetY(unsigned int pos)
{
	return (int)((pos >> 16) & 0xFFFF);
}

bool CHitFindingOperation::NeighborhoodON(CBlobData *redBlob, int i, int j)
{
	if (((i - 1) >= 0) && ((j - 1) >= 0) && (redBlob->m_Map[redBlob->m_Width * (i - 1) + (j - 1)] == (unsigned char)255))
		return true;
	if (((i - 1) >= 0) && (redBlob->m_Map[redBlob->m_Width * (i - 1) + j] == (unsigned char)255))
		return true;
	if (((i - 1) >= 0) && ((j + 1) < redBlob->m_Width) && (redBlob->m_Map[redBlob->m_Width * (i - 1) + (j + 1)] == (unsigned char)255))
		return true;
	if (((j - 1) >= 0) && (redBlob->m_Map[redBlob->m_Width * i + (j - 1)] == (unsigned char)255))
		return true;
	if (redBlob->m_Map[redBlob->m_Width * i + j] == (unsigned char)255)
		return true;
	if (((j + 1) < redBlob->m_Width) && (redBlob->m_Map[redBlob->m_Width * i + (j + 1)] == (unsigned char)255))
		return true;
	if (((i + 1) < redBlob->m_Height) && ((j - 1) >= 0) && (redBlob->m_Map[redBlob->m_Width * (i + 1) + (j - 1)] == (unsigned char)255))
		return true;
	if (((i + 1) < redBlob->m_Height) && (redBlob->m_Map[redBlob->m_Width * (i + 1) + j] == (unsigned char)255))
		return true;
	if (((i + 1) < redBlob->m_Height) && ((j + 1) < redBlob->m_Width) && (redBlob->m_Map[redBlob->m_Width * (i + 1) + (j + 1)] == (unsigned char)255))
		return true;
	return false;
}

int CHitFindingOperation::GetAspectRatio(CBlobData *blob, CRGNData *ptr)
{
	int aspectRatio = 0;

	if (blob->m_Boundary->size() > 0)
	{
		int maxDX = 0;
		int maxDY = 0;
		int maxDistance = 0;
		int maxi = 0;
		int maxj = 0;
		int maxX0 = 0;
		int maxY0 = 0;
		int maxX1 = 0;
		int maxY1 = 0;
		for (int i = 0; i < (int)(blob->m_Boundary->size() - 1); i++)
		{
			unsigned int pos = (*blob->m_Boundary)[i];
			int x0 = GetX(pos);
			int y0 = GetY(pos);
			for (int j = i + 1; j < (int)blob->m_Boundary->size(); j++)
			{
				pos = (*blob->m_Boundary)[j];
				int x1 = GetX(pos);
				int y1 = GetY(pos);
				int dx = x1 - x0;
				int dy = y1 - y0;
				int distance = dx * dx + dy * dy;
				if (distance > maxDistance)
				{
					maxDistance = distance;
					maxDX = dx;
					maxDY = dy;
					maxi = i;
					maxj = j;
					maxX0 = x0;
					maxY0 = y0;
					maxX1 = x1;
					maxY1 = y1;
				}
			}
		}
		double maxDistance2 = sqrt(maxDistance);
		ptr->m_LongAxisLength = (int) maxDistance2;
		int maxDistance1 = 0;
		for (int i = 0; i < (int)blob->m_Boundary->size(); i++)
		{
			if ((i == maxi) || (i == maxj))
				continue;
			unsigned int pos = (*blob->m_Boundary)[i];
			int x0 = GetX(pos);
			int y0 = GetY(pos);
			int distance = (int) (abs(maxDY * x0 - maxDX * y0 + maxX1 * maxY0 - maxY1 * maxX0) / maxDistance2);
			if (distance > maxDistance1)
				maxDistance1 = distance;
		}
		maxDistance1 = 2 * maxDistance1;
		if (maxDistance1 > ptr->m_LongAxisLength)
		{
			maxDistance = maxDistance1;
			maxDistance1 = ptr->m_LongAxisLength;
			ptr->m_LongAxisLength = maxDistance;
		}
		aspectRatio = 100 * maxDistance1 / ptr->m_LongAxisLength;
	}
	
	return aspectRatio;
}
