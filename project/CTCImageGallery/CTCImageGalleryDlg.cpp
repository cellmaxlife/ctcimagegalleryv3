﻿
// CTCImageGalleryDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTCImageGalleryDlg.h"
#include "afxdialogex.h"
#include "VersionNumber.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define IMAGE_WINDOW_X0 13
#define IMAGE_WINDOW_Y0 56
#define IMAGE_WINDOW_X1 1704
#define IMAGE_WINDOW_Y1 987

#define SAMPLEFRAMEWIDTH  100
#define SAMPLEFRAMEHEIGHT 100
#define SAMPLEFRAMEWIDTH2  50
#define SAMPLEFRAMEHEIGHT2 50

// CCTCImageGalleryDlg dialog
static DWORD WINAPI SortRegionsByScore(LPVOID param)
{
	CCTCImageGalleryDlg *ptr = (CCTCImageGalleryDlg *)param;
	ptr->RankByScore();
	ptr->m_WaitDialog->CloseDialog();
	return 0;
}

CCTCImageGalleryDlg::CCTCImageGalleryDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CCTCImageGalleryDlg::IDD, pParent)
	, m_ImageFilename(_T(""))
	, m_RegionFilename(_T(""))
	, m_TotalRegions(0)
	, m_TotalPages(0)
	, m_TotalSelected(0)
	, m_CurrentPage(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_RedIntensity = 200;
	m_GreenIntensity = 200;
	m_BlueIntensity = 200;
	m_ImageWidth = 100;
	m_ImageHeight = 100;
	m_Image.Create((NUM_IMAGE_COLUMNS * m_ImageWidth), -(NUM_IMAGE_ROWS * m_ImageHeight), 24);
	m_WaitDialog = new CWaitDialog();
}

void CCTCImageGalleryDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_ImageFilename);
	DDX_Text(pDX, IDC_EDIT2, m_RegionFilename);
	DDX_Text(pDX, IDC_TOTALCTCS, m_TotalSelected);
	DDX_Control(pDX, IDC_IMAGE, m_HitImage);
	DDX_Text(pDX, IDC_TOTALREGIONS, m_TotalRegions);
	DDX_Text(pDX, IDC_TOTALPAGES, m_TotalPages);
	DDX_Text(pDX, IDC_PAGENUM, m_CurrentPage);
	DDX_Control(pDX, IDC_BLUECOLOR, m_BlueColor);
	DDX_Control(pDX, IDC_GREENCOLOR, m_GreenColor);
	DDX_Control(pDX, IDC_REDCOLOR, m_RedColor);
	DDX_Control(pDX, IDC_SHOW, m_ShowBoundary);
	DDX_Control(pDX, IDC_NOTSHOW, m_NotShowBoundary);
}

BEGIN_MESSAGE_MAP(CCTCImageGalleryDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_LOADIMG, &CCTCImageGalleryDlg::OnBnClickedLoadimg)
	ON_BN_CLICKED(IDC_LOADRGN, &CCTCImageGalleryDlg::OnBnClickedLoadrgn)
	ON_BN_CLICKED(IDC_NEXTVIEW, &CCTCImageGalleryDlg::OnBnClickedNextview)
	ON_BN_CLICKED(IDC_SAVERGN, &CCTCImageGalleryDlg::OnBnClickedSavergn)
	ON_BN_CLICKED(IDCANCEL, &CCTCImageGalleryDlg::OnBnClickedCancel)
	ON_WM_LBUTTONDOWN()
	ON_BN_CLICKED(IDC_PREVVIEW, &CCTCImageGalleryDlg::OnBnClickedPrevview)
	ON_BN_CLICKED(IDC_GOTO, &CCTCImageGalleryDlg::OnBnClickedGoto)
	ON_BN_CLICKED(IDC_REDCOLOR, &CCTCImageGalleryDlg::OnBnClickedRedcolor)
	ON_BN_CLICKED(IDC_GREENCOLOR, &CCTCImageGalleryDlg::OnBnClickedGreencolor)
	ON_BN_CLICKED(IDC_BLUECOLOR, &CCTCImageGalleryDlg::OnBnClickedBluecolor)
	ON_BN_CLICKED(IDC_SHOW, &CCTCImageGalleryDlg::OnBnClickedShow)
	ON_BN_CLICKED(IDC_NOTSHOW, &CCTCImageGalleryDlg::OnBnClickedNotshow)
END_MESSAGE_MAP()


// CCTCImageGalleryDlg message handlers

BOOL CCTCImageGalleryDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	CString version;
	version.Format(_T("CTCImageGallery(%s)"), CELLMAPPLUS_VERSION);
	m_Log.NewLog(version);
	SetWindowText(version);
	version.Format(_T("LeicaParams%s.txt"), CELLMAPPLUS_VERSION);
	m_CTCParams.LoadCTCParams(version);
	m_RedTIFFData = new CSingleChannelTIFFData();
	m_GreenTIFFData = new CSingleChannelTIFFData();
	m_BlueTIFFData = new CSingleChannelTIFFData();
	m_RedColor.SetCheck(BST_CHECKED);
	m_GreenColor.SetCheck(BST_CHECKED);
	m_BlueColor.SetCheck(BST_CHECKED);
	m_NotShowBoundary.SetCheck(BST_UNCHECKED);
	m_ShowBoundary.SetCheck(BST_CHECKED);
	EnableButtons(FALSE);
	CButton *btn = (CButton *)GetDlgItem(IDC_LOADIMG);
	btn->EnableWindow(TRUE);
	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CCTCImageGalleryDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
		PaintCImages();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CCTCImageGalleryDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CCTCImageGalleryDlg::UpdateImageDisplay()
{
	RECT rect;
	m_HitImage.GetWindowRect(&rect);
	ScreenToClient(&rect);
	InvalidateRect(&rect, false);
}

void CCTCImageGalleryDlg::EnableButtons(BOOL enable)
{
	CButton *btn = (CButton *)GetDlgItem(IDC_LOADIMG);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_LOADRGN);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_NEXTVIEW);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_PREVVIEW);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_SAVERGN);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_GOTO);
	btn->EnableWindow(enable);
}

void CCTCImageGalleryDlg::OnBnClickedLoadimg()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	EnableButtons(FALSE);
	CButton *btn = (CButton *)GetDlgItem(IDC_LOADIMG);

	const CString LEICA_RED_FILEPREFIX = _T("TRITC-Rhoadmine Selection");
	const CString LEICA_GREEN_FILEPREFIX = _T("FITC Selection");
	const CString LEICA_BLUE_FILEPREFIX = _T("DAPI Selection");
	const CString ZEISS_RED_POSTFIX = _T("_c3_ORG.tif");
	const CString ZEISS_GREEN_POSTFIX = _T("_c4_ORG.tif");
	const CString ZEISS_BLUE_POSTFIX = _T("_c1_ORG.tif");
	FreeRGNList(&m_AutoRGNDataList);
	if (m_Image != NULL)
	{
		m_Image.Destroy();
		m_Image.Create((NUM_IMAGE_COLUMNS * m_ImageWidth), -(NUM_IMAGE_ROWS * m_ImageHeight), 24);
	}
	ResetAllCounts();
	m_RegionFilename = _T("");
	UpdateData(FALSE);
	CString message;

	CFileDialog dlg(TRUE,    // open
		NULL,    // no default extension
		NULL,    // no initial file name
		OFN_FILEMUSTEXIST
		| OFN_HIDEREADONLY,
		_T("TIFF files (*.tif)|*.tif||"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		CString filename = dlg.GetPathName();
		m_ImageFilename = dlg.GetFileName();
		UpdateData(FALSE);
		int index = m_ImageFilename.Find(LEICA_RED_FILEPREFIX, 0);
		if (index == -1)
		{
			index = m_ImageFilename.Find(ZEISS_RED_POSTFIX, 0);
		}
		if (index == -1)
		{
			message.Format(_T("Image Filename %s doesn't have either Leica Image Prefix %s or Zeiss Image Postfix %s"), m_ImageFilename, LEICA_RED_FILEPREFIX, ZEISS_RED_POSTFIX);
			AfxMessageBox(message);
			btn = (CButton *)GetDlgItem(IDC_LOADIMG);
			btn->EnableWindow(TRUE);
			return;
		}
		bool ret = m_RedTIFFData->LoadRawTIFFFile(filename);
		if (!ret)
		{	
			message.Format(_T("Failed to load %s"), m_ImageFilename);
			AfxMessageBox(message);
			btn = (CButton *)GetDlgItem(IDC_LOADIMG);
			btn->EnableWindow(TRUE);
			return;
		}
		else
		{
			message.Format(_T("Red TIFF File %s loaded successfully, Red CPI=%d"), filename, m_RedTIFFData->GetCPI());
			m_Log.Message(message);
			index = m_ImageFilename.Find(LEICA_RED_FILEPREFIX, 0);
			if (index == -1)
			{
				int postfixIndex = filename.Find(ZEISS_RED_POSTFIX);
				if (postfixIndex == -1)
				{
					message.Format(_T("Image Filename %s doesn't have either Leica Image Prefix %s or Zeiss Image Postfix %s"), m_ImageFilename, LEICA_RED_FILEPREFIX, ZEISS_RED_POSTFIX);
					AfxMessageBox(message);
					btn = (CButton *)GetDlgItem(IDC_LOADIMG);
					btn->EnableWindow(TRUE);
					return;
				}
				else
				{
					CString samplePathName = filename.Mid(0, postfixIndex);
					postfixIndex = m_ImageFilename.Find(ZEISS_RED_POSTFIX);
					CString sampleName = m_ImageFilename.Mid(0, postfixIndex);
					CString filename2 = samplePathName + ZEISS_GREEN_POSTFIX;
					BOOL ret = m_GreenTIFFData->LoadRawTIFFFile(filename2);
					if (ret)
					{
						message.Format(_T("Green TIFF File %s loaded successfully, Green CPI=%d"), filename, m_GreenTIFFData->GetCPI());
						m_Log.Message(message);
						filename2 = samplePathName + ZEISS_BLUE_POSTFIX;
						BOOL ret = m_BlueTIFFData->LoadRawTIFFFile(filename2);
						if (ret)
						{
							message.Format(_T("Blue TIFF File %s loaded successfully, Green CPI=%d"), filename, m_BlueTIFFData->GetCPI());
							m_Log.Message(message);
							message.Format(_T("ZeissParams%s.txt"), CELLMAPPLUS_VERSION);
							m_CTCParams.LoadCTCParams(message);
							btn = (CButton *)GetDlgItem(IDC_LOADRGN);
							btn->EnableWindow(TRUE);
						}
						else
						{
							message.Format(_T("Failed to load Blue TIFF File %s"), filename2);
							AfxMessageBox(message);
							btn = (CButton *)GetDlgItem(IDC_LOADIMG);
							btn->EnableWindow(TRUE);
							return;
						}
					}
					else
					{
						message.Format(_T("Failed to load Green TIFF File %s"), filename2);
						AfxMessageBox(message);
						btn = (CButton *)GetDlgItem(IDC_LOADIMG);
						btn->EnableWindow(TRUE);
						return;
					}
				}
			}
			else
			{
				CString postfix = m_ImageFilename.Mid(LEICA_RED_FILEPREFIX.GetLength());
				index = filename.Find(LEICA_RED_FILEPREFIX, 0);
				CString pathname = filename.Mid(0, index);
				CString filename2;
				filename2.Format(_T("%s%s%s"), pathname, LEICA_GREEN_FILEPREFIX, postfix);
				ret = m_GreenTIFFData->LoadRawTIFFFile(filename2);
				if (!ret)
				{
					message.Format(_T("Failed to load Green TIFF File %s"), filename2);
					AfxMessageBox(message);
					btn = (CButton *)GetDlgItem(IDC_LOADIMG);
					btn->EnableWindow(TRUE);
					return;
				}
				else
				{
					message.Format(_T("Green TIFF File %s loaded successfully, Green CPI=%d"), filename2, m_GreenTIFFData->GetCPI());
					m_Log.Message(message);
					pathname = filename.Mid(0, index);
					filename2.Format(_T("%s%s%s"), pathname, LEICA_BLUE_FILEPREFIX, postfix);
					ret = m_BlueTIFFData->LoadRawTIFFFile(filename2);
					if (!ret)
					{
						message.Format(_T("Failed to load Blue TIFF File %s"), filename2);
						AfxMessageBox(message);
						btn = (CButton *)GetDlgItem(IDC_LOADIMG);
						btn->EnableWindow(TRUE);
						return;
					}
					else
					{
						message.Format(_T("Blue TIFF File %s loaded successfully, Blue CPI=%d"), filename2, m_BlueTIFFData->GetCPI());
						m_Log.Message(message);
						message.Format(_T("LeicaParams%s.txt"), CELLMAPPLUS_VERSION);
						m_CTCParams.LoadCTCParams(message);
						btn = (CButton *)GetDlgItem(IDC_LOADRGN);
						btn->EnableWindow(TRUE);
					}
				}
			}
		}
	}
	btn = (CButton *)GetDlgItem(IDC_LOADIMG);
	btn->EnableWindow(TRUE);
}

void CCTCImageGalleryDlg::OnBnClickedLoadrgn()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CButton *btn = (CButton *)GetDlgItem(IDC_LOADRGN);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_NEXTVIEW);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_PREVVIEW);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_SAVERGN);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_LOADIMG);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_GOTO);
	btn->EnableWindow(FALSE);
	FreeRGNList(&m_AutoRGNDataList);
	if (m_Image != NULL)
	{
		m_Image.Destroy();
		m_Image.Create((NUM_IMAGE_COLUMNS * m_ImageWidth), -(NUM_IMAGE_ROWS * m_ImageHeight), 24);
	}
	ResetAllCounts();
	m_RegionFilename = _T("");
	UpdateData(FALSE);
	m_RedIntensity = m_RedTIFFData->GetCPI();
	m_GreenIntensity = m_GreenTIFFData->GetCPI();
	m_BlueIntensity = m_BlueTIFFData->GetCPI();
	CString message;
	for (int i = 0; i <= (int)PARAM_LASTONE; i++)
	{
		message.Format(_T("%s=%d"), m_CTCParams.m_CTCParamNames[i], m_CTCParams.m_CTCParams[i]);
		m_Log.Message(message);
	}

	CFileDialog dlg(TRUE,    // open
		NULL,    // no default extension
		NULL,    // no initial file name
		OFN_FILEMUSTEXIST
		| OFN_HIDEREADONLY,
		_T("Region files (*.rgn, *.tmp)|*.rgn; *.tmp"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		CString filename = dlg.GetPathName();
		m_RegionFilename = dlg.GetFileName();
		UpdateData(FALSE);
		if (LoadRegionData(filename))
		{
			message.Format(_T("Region File %s loaded successfully, #Hits=%d"), filename, m_AutoRGNDataList.size());
			m_Log.Message(message);
			m_TotalRegions = (int) m_AutoRGNDataList.size();
			if (m_TotalRegions > 0)
			{
				m_TotalPages = (m_TotalRegions / NUM_IMAGES);
				m_TotalPages++;
				if ((m_TotalRegions % NUM_IMAGES) == 0)
					m_TotalPages++;
				if (m_TotalPages >= 1)
					m_CurrentPage = 1;
				UpdateData(FALSE);

				HANDLE thread = ::CreateThread(NULL, 0, SortRegionsByScore, this, CREATE_SUSPENDED, NULL);

				if (!thread)
				{
					AfxMessageBox(_T("Fail to create thread for scoring"));
					message.Format(_T("Failed to creat a thread for scoring"));
					m_Log.Message(message);
					return;
				}

				::SetThreadPriority(thread, THREAD_PRIORITY_NORMAL);
				::ResumeThread(thread);
				message.Format(_T("Launched a thread for scoring"));
				m_Log.Message(message);
				m_WaitDialog->DoModal();
				DisplayCellRegions();
				btn = (CButton *)GetDlgItem(IDC_SAVERGN);
				btn->EnableWindow(TRUE);
			}
			message.Format(_T("TotalPages=%d, CurrentPage=%d"), m_TotalPages, m_CurrentPage);
			m_Log.Message(message);
			if (m_CurrentPage < m_TotalPages)
			{
				btn = (CButton *)GetDlgItem(IDC_NEXTVIEW);
				btn->EnableWindow(TRUE);
				btn = (CButton *)GetDlgItem(IDC_GOTO);
				btn->EnableWindow(TRUE);
			}
		}
		else
		{
			message.Format(_T("Failed to load Auto Region File %s"), m_RegionFilename);
			AfxMessageBox(message);
		}
	}
	btn = (CButton *)GetDlgItem(IDC_LOADRGN);
	btn->EnableWindow(TRUE);
	btn = (CButton *)GetDlgItem(IDC_LOADIMG);
	btn->EnableWindow(TRUE);
}


void CCTCImageGalleryDlg::OnBnClickedNextview()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CButton *btn = (CButton *)GetDlgItem(IDC_NEXTVIEW);
	btn->EnableWindow(FALSE);
	if (m_Image != NULL)
	{
		m_Image.Destroy();
		m_Image.Create((NUM_IMAGE_COLUMNS * m_ImageWidth), -(NUM_IMAGE_ROWS * m_ImageHeight), 24);
	}
	if (m_CurrentPage < m_TotalPages)
	{
		SaveTempRegionFile();
		m_CurrentPage++;
		UpdateData(FALSE);
		DisplayCellRegions();
		btn = (CButton *)GetDlgItem(IDC_PREVVIEW);
		btn->EnableWindow(TRUE);
	}
	if (m_CurrentPage < m_TotalPages)
	{
		btn = (CButton *)GetDlgItem(IDC_NEXTVIEW);
		btn->EnableWindow(TRUE);
	}
}

void CCTCImageGalleryDlg::OnBnClickedPrevview()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CButton *btn = (CButton *)GetDlgItem(IDC_PREVVIEW);
	btn->EnableWindow(FALSE);
	if (m_Image != NULL)
	{
		m_Image.Destroy();
		m_Image.Create((NUM_IMAGE_COLUMNS * m_ImageWidth), -(NUM_IMAGE_ROWS * m_ImageHeight), 24);
	}
	if (m_CurrentPage > 1)
	{
		SaveTempRegionFile();
		m_CurrentPage--;
		UpdateData(FALSE);
		DisplayCellRegions();
		btn = (CButton *)GetDlgItem(IDC_NEXTVIEW);
		btn->EnableWindow(TRUE);
	}
	if (m_CurrentPage > 1)
	{
		btn = (CButton *)GetDlgItem(IDC_PREVVIEW);
		btn->EnableWindow(TRUE);
	}
}

void CCTCImageGalleryDlg::OnBnClickedSavergn()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	if (m_TotalSelected > 0)
	{
		CString filenameForSave;

		CString filename = m_RegionFilename;
		WCHAR *char1 = _T(".");
		int fileStringIndex = filename.ReverseFind(*char1);
		filename = filename.Mid(0, fileStringIndex);
		filename += _T("_Selection.rgn");
		
		CFileDialog dlg(FALSE,    // save
			CString(".rgn"),    
			filename,    
			OFN_OVERWRITEPROMPT,
			_T("RGN files (*.rgn)|*.rgn||"), NULL, 0, TRUE);
		if (dlg.DoModal() == IDOK)
		{
			filenameForSave = dlg.GetPathName();
			m_RegionFilename = dlg.GetFileName();
			UpdateData(FALSE);

			SaveRegionFile(filenameForSave);
		}
	}
}

void  CCTCImageGalleryDlg::PaintCImages()
{

	CPaintDC dc(&m_HitImage);
	CRect rect;
	m_HitImage.GetClientRect(&rect);
	dc.SetStretchBltMode(HALFTONE);
	m_Image.StretchBlt(dc.m_hDC, rect);
	CDC* pDC = m_HitImage.GetDC();
	CPen CursorPen(PS_SOLID, 3, RGB(255, 255, 255));
	CPen *pOldPen = pDC->SelectObject(&CursorPen);
	int rectWidth = rect.right - rect.left + 1;
	int rectHeight = rect.bottom - rect.top + 1;
	int blockWidth = rectWidth / NUM_IMAGE_COLUMNS;
	int blockHeight = rectHeight / NUM_IMAGE_ROWS;
	for (int i = 1; i < NUM_IMAGE_ROWS; i++)
	{
		pDC->MoveTo(rect.left, rect.top + i * blockHeight);
		pDC->LineTo(rect.right, rect.top + i * blockHeight);
	}
	for (int i = 1; i < NUM_IMAGE_COLUMNS; i++)
	{
		pDC->MoveTo(rect.left + i * blockWidth, rect.top);
		pDC->LineTo(rect.left + i * blockWidth, rect.bottom);
	}
	CPen CursorPen1(PS_SOLID, 5, RGB(255, 0, 0));
	pDC->SelectObject(&CursorPen1);
	if ((m_CurrentPage >= 1) && (m_CurrentPage <= m_TotalPages))
	{
		int regionIndex = (m_CurrentPage - 1) * NUM_IMAGES;
		int totalSelectables = m_TotalRegions;
		for (int i = 0; i < NUM_IMAGES; i++, regionIndex++)
		{
			int ii = i / NUM_IMAGE_COLUMNS;
			int jj = i % NUM_IMAGE_COLUMNS;	
			CRect rect1;
			rect1.left = rect.left + jj * blockWidth + 10;
			rect1.top = rect.top + ii * blockHeight + 10;
			rect1.right = rect1.left + 200;
			rect1.bottom = rect1.top + 20;
			pDC->SetTextColor(RGB(255, 255, 255));
			pDC->SetBkMode(TRANSPARENT);
			pDC->SetTextAlign(TA_LEFT);
			CString label;
			if ((regionIndex >= 0) && (regionIndex < totalSelectables))
			{
				label.Format(_T("%d"), regionIndex+1);
				if (m_AutoRGNDataList[regionIndex]->GetColorCode() == CTC)
				{
					pDC->SetTextColor(RGB(255, 0, 0));
				}					
			}
			pDC->DrawTextW(label, &rect1, DT_SINGLELINE | DT_NOCLIP);

			
			if ((regionIndex >= 0) && (regionIndex < totalSelectables))
			{
				if (m_AutoRGNDataList[regionIndex]->GetColorCode() == CTC)
				{
					int x0 = rect.left + jj * blockWidth + 140;
					int y0 = rect.top + ii * blockHeight + 40;
					pDC->MoveTo(x0, y0);
					pDC->LineTo(x0 + 10, y0 + 20);
					pDC->LineTo(x0 + 30, y0 - 30);
				}
			}
		}
	}

	pDC->SelectObject(pOldPen);
}

void CCTCImageGalleryDlg::OnBnClickedCancel()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	FreeRGNList(&m_AutoRGNDataList);
	delete m_RedTIFFData;
	delete m_GreenTIFFData;
	delete m_BlueTIFFData;
	delete m_WaitDialog;
	CDialogEx::OnCancel();
}

bool CCTCImageGalleryDlg::LoadRegionData(CString filename)
{
	bool ret = false;
	const CString TAG1 = _T("0 1, 1 ");
	const CString TAG2 = _T(", 2 ");
	const CString TAG3 = _T(", ");

	vector<CRGNData *> *list = NULL;
	list = &m_AutoRGNDataList;
	FreeRGNList(list);

	bool tmpFile = false;
	int index = filename.Find(_T(".tmp"));
	if (index > 0)
		tmpFile = true;

	CStdioFile theFile;
	if (theFile.Open(filename, CFile::modeRead | CFile::typeText))
	{
		CString textline;
		while (theFile.ReadString(textline))
		{
			int index = textline.Find(TAG1);
			if (index > -1)
			{
				textline = textline.Mid(TAG1.GetLength());
				index = textline.Find(TAG2);
				if (index > -1)
				{
					unsigned int color = _wtoi(textline.Mid(0, index));
					textline = textline.Mid(index + TAG2.GetLength());
					index = textline.Find(_T(" "));
					if (index > -1)
					{
						int x0 = _wtoi(textline.Mid(0, index));
						textline = textline.Mid(index + 1);
						index = textline.Find(TAG3);
						if (index > -1)
						{
							ret = true;
							int y0 = _wtoi(textline.Mid(0, index));
							CRGNData *data = new CRGNData(x0, y0, SAMPLEFRAMEWIDTH, SAMPLEFRAMEHEIGHT);
							if (tmpFile)
								data->SetColorCode(color);
							else
								data->SetColorCode(NONCTC);
							data->SetCPI(RED_COLOR, m_RedIntensity);
							data->SetCPI(GREEN_COLOR, m_GreenIntensity);
							data->SetCPI(BLUE_COLOR, m_BlueIntensity);
							data->SetCutoff(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CUTOFF]);
							data->SetCutoff(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CUTOFF]);
							data->SetCutoff(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CUTOFF]);
							data->SetContrast(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CONTRAST]);
							data->SetContrast(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CONTRAST]);
							data->SetContrast(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CONTRAST]);
							data->SetThreshold(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_THRESHOLD]);
							data->SetThreshold(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_THRESHOLD]);
							data->SetThreshold(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_THRESHOLD]);
							list->push_back(data);
							continue;
						}
						else
						{
							ret = false;
							break;
						}
					}
					else
					{
						ret = false;
						break;
					}
				}
				else
				{
					ret = false;
					break;
				}
			}
			else
			{
				ret = false;
				break;
			}
		}
		theFile.Close();
	}
	return ret;
}

void CCTCImageGalleryDlg::FreeRGNList(vector<CRGNData *> *rgnList)
{
	for (int i = 0; i < (int)rgnList->size(); i++)
	{
		if ((*rgnList)[i] != NULL)
		{
			delete (*rgnList)[i];
			(*rgnList)[i] = NULL;
		}
	}
	(*rgnList).clear();
}

void CCTCImageGalleryDlg::DisplayCellRegions()
{
	if ((m_CurrentPage >= 1) && (m_CurrentPage <= m_TotalPages))
	{
		int regionIndex = (m_CurrentPage - 1) * NUM_IMAGES;
		int totalSelectables = m_TotalRegions;
		for (int i = 0; i < NUM_IMAGES; i++, regionIndex++)
		{
			if ((regionIndex >= 0) && (regionIndex < totalSelectables))
			{
				CRGNData *ptr = m_AutoRGNDataList[regionIndex];
				CopyToRGBImage(ptr, i);
			}
			else
			{
				unsigned short *redImg = new unsigned short[m_ImageWidth * m_ImageHeight];
				unsigned short *greenImg = new unsigned short[m_ImageWidth * m_ImageHeight];
				unsigned short *blueImg = new unsigned short[m_ImageWidth * m_ImageHeight];
				memset(redImg, 0, sizeof(unsigned short) * m_ImageWidth * m_ImageHeight);
				memset(greenImg, 0, sizeof(unsigned short) * m_ImageWidth * m_ImageHeight);
				memset(blueImg, 0, sizeof(unsigned short) * m_ImageWidth * m_ImageHeight);
				CRGNData *ptr1 = new CRGNData(0, 0, m_ImageWidth, m_ImageHeight);
				ptr1->SetImages(redImg, greenImg, blueImg);
				CopyToRGBImage(ptr1, i);
				delete ptr1;
			}
		}
	}
	else
	{
		if (m_Image != NULL)
		{
			m_Image.Destroy();
			m_Image.Create((NUM_IMAGE_COLUMNS * m_ImageWidth), -(NUM_IMAGE_ROWS * m_ImageHeight), 24);
		}
	}
	UpdateImageDisplay();
}

void CCTCImageGalleryDlg::CopyToRGBImage(CRGNData *rgnPtr, int ImageLocationIndex)
{
	BYTE *pCursor = (BYTE*)m_Image.GetBits();
	int nPitch = m_Image.GetPitch();

	unsigned short *pBlueBuffer = rgnPtr->GetImage(BLUE_COLOR);
	unsigned short *pGreenBuffer = rgnPtr->GetImage(GREEN_COLOR);
	unsigned short *pRedBuffer = rgnPtr->GetImage(RED_COLOR);
	int regionWidth = rgnPtr->GetWidth();
	int regionHeight = rgnPtr->GetHeight();

	int blueMax = rgnPtr->GetContrast(BLUE_COLOR) + rgnPtr->GetCPI(BLUE_COLOR);
	if (rgnPtr->m_BlueFrameMax > 0)
		blueMax = rgnPtr->m_BlueFrameMax;
	int redMax = rgnPtr->GetContrast(RED_COLOR) + rgnPtr->GetCPI(RED_COLOR);
	if (rgnPtr->m_RedFrameMax > 0)
		redMax = rgnPtr->m_RedFrameMax;

	BYTE bluePixelValue = 0;
	BYTE greenPixelValue = 0;
	BYTE redPixelValue = 0;

	for (int y = 0; y < regionHeight; y++)
	{
		for (int x = 0; x < regionWidth; x++)
		{
			bluePixelValue = GetContrastEnhancedByte(*pBlueBuffer++, blueMax, rgnPtr->GetCutoff(BLUE_COLOR) + rgnPtr->GetCPI(BLUE_COLOR));
			greenPixelValue = GetContrastEnhancedByte(*pGreenBuffer++, rgnPtr->GetContrast(GREEN_COLOR) + rgnPtr->GetCPI(GREEN_COLOR), rgnPtr->GetCutoff(GREEN_COLOR) + rgnPtr->GetCPI(GREEN_COLOR));
			redPixelValue = GetContrastEnhancedByte(*pRedBuffer++, redMax, rgnPtr->GetCutoff(RED_COLOR) + rgnPtr->GetCPI(RED_COLOR));
			if (m_RedColor.GetCheck() == BST_UNCHECKED)
				redPixelValue = (BYTE)0;
			if (m_GreenColor.GetCheck() == BST_UNCHECKED)
				greenPixelValue = (BYTE)0;
			if (m_BlueColor.GetCheck() == BST_UNCHECKED)
				bluePixelValue = (BYTE)0;
			pCursor[nPitch * (m_ImageHeight * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y) + 3 * (m_ImageWidth * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x)] = bluePixelValue;
			pCursor[nPitch * (m_ImageHeight * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y) + 3 * (m_ImageWidth * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x) + 1] = greenPixelValue;
			pCursor[nPitch * (m_ImageHeight * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y) + 3 * (m_ImageWidth * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x) + 2] = redPixelValue;
		}
	}

	if (m_ShowBoundary.GetCheck() == BST_CHECKED)
	{
		vector<CBlobData *> *redBlobs = rgnPtr->GetBlobData(RED_COLOR);

		if (redBlobs->size() > 0)
		{
			for (int i = 0; i < (int)redBlobs->size(); i++)
			{
				for (int j = 0; j < (int)(*redBlobs)[i]->m_Boundary->size(); j++)
				{
					unsigned int pos = (*(*redBlobs)[i]->m_Boundary)[j];
					int x0 = m_HitFinder.GetX(pos);
					int y0 = m_HitFinder.GetY(pos);
					pCursor[nPitch * (m_ImageHeight * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (m_ImageWidth * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0)] = (BYTE)255;
					pCursor[nPitch * (m_ImageHeight * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (m_ImageWidth * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0) + 1] = (BYTE)255;
					pCursor[nPitch * (m_ImageHeight * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (m_ImageWidth * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0) + 2] = (BYTE)255;
				}
			}
		}
	}
}

BYTE CCTCImageGalleryDlg::GetContrastEnhancedByte(unsigned short value, unsigned short contrast, int cutoff)
{
	BYTE result = 0;
	int value1 = value - cutoff;
	int maxValue = (int)contrast;
	maxValue -= cutoff;
	if (maxValue < 255)
		maxValue = 255;

	if (value1 < 0)
		result = (BYTE)0;
	else if (value1 > maxValue)
		result = (BYTE)255;
	else
	{
		result = (BYTE)(255.0 * value1 / maxValue);;
	}
	return result;
}

void CCTCImageGalleryDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO:  在此加入您的訊息處理常式程式碼和 (或) 呼叫預設值
	int xLoc = -1;
	int xInterval = (IMAGE_WINDOW_X1 - IMAGE_WINDOW_X0) / NUM_IMAGE_COLUMNS;
	for (int i = 0; i < NUM_IMAGE_COLUMNS; i++)
	{
		if ((point.x >= (IMAGE_WINDOW_X0 + i * xInterval)) &&
			(point.x < (IMAGE_WINDOW_X0 + (i + 1) * xInterval)))
		{
			xLoc = i;
			break;
		}
	}
	int yLoc = -1;
	int yInterval = (IMAGE_WINDOW_Y1 - IMAGE_WINDOW_Y0) / NUM_IMAGE_ROWS;
	for (int i = 0; i < NUM_IMAGE_ROWS; i++)
	{
		if ((point.y >= (IMAGE_WINDOW_Y0 + i * yInterval)) &&
			(point.y < (IMAGE_WINDOW_Y0 + (i + 1) * yInterval)))
		{
			yLoc = i;
			break;
		}
	}
	if ((xLoc > -1) && (yLoc > -1))
	{
		int totalSelectables = m_TotalRegions;
		int ImageLocationIndex = NUM_IMAGE_COLUMNS * yLoc + xLoc;
		int index = (m_CurrentPage - 1) * NUM_IMAGES + ImageLocationIndex;
		if ((index >= 0) && (index < totalSelectables))
		{
			if (m_AutoRGNDataList[index]->GetColorCode() == CTC)
			{
				m_AutoRGNDataList[index]->SetColorCode(NONCTC);
				m_TotalSelected--;
			}
			else
			{
				m_AutoRGNDataList[index]->SetColorCode(CTC);
				m_TotalSelected++;
			}
			UpdateData(FALSE);
			UpdateImageDisplay();
		}
	}
	CDialogEx::OnLButtonDblClk(nFlags, point);
}

void CCTCImageGalleryDlg::ProcessOneRegionForDisplay(CRGNData *ptr)
{
	unsigned int color = ptr->GetColorCode();
	vector<CRGNData *> cellList;
	m_HitFinder.ProcessOneROI(&m_CTCParams, ptr, &cellList);
	if (cellList.size() > 0)
	{
		m_HitFinder.GetCenterCell(ptr, &cellList, -1, -1, NULL, 1);
		FreeRGNList(&cellList);
	}
	ptr->SetColorCode(color);
}

void CCTCImageGalleryDlg::OnBnClickedGoto()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	UpdateData(TRUE);
	if ((m_CurrentPage >= 1) && (m_CurrentPage <= m_TotalPages))
	{
		SaveTempRegionFile();
		DisplayCellRegions();
	}
}

BOOL CCTCImageGalleryDlg::PreTranslateMessage(MSG* pMsg)
{
	CEdit *pageNum = (CEdit *)GetDlgItem(IDC_PAGENUM);
	
	if ((pMsg->message == WM_KEYDOWN) &&
		(pMsg->wParam == VK_RETURN) &&
		(GetFocus() == pageNum))
	{
		return TRUE; 
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CCTCImageGalleryDlg::OnBnClickedRedcolor()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	DisplayCellRegions();
}


void CCTCImageGalleryDlg::OnBnClickedGreencolor()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	DisplayCellRegions();
}


void CCTCImageGalleryDlg::OnBnClickedBluecolor()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	DisplayCellRegions();
}


void CCTCImageGalleryDlg::OnBnClickedShow()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	DisplayCellRegions();
}


void CCTCImageGalleryDlg::OnBnClickedNotshow()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	DisplayCellRegions();
}

void CCTCImageGalleryDlg::ResetAllCounts()
{
	m_TotalPages = 0;
	m_TotalRegions = 0;
	m_TotalSelected = 0;
	m_CurrentPage = 0;
}

void CCTCImageGalleryDlg::SaveRegionFile(CString filename)
{
	CStdioFile theFile;

	bool tmpFile = false;
	int index = filename.Find(_T(".tmp"));
	if (index > 0)
		tmpFile = true;

	if (theFile.Open(filename, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
	{
		int count = 0;
		for (int i = 0; i < (int)m_AutoRGNDataList.size(); i++)
		{
			if ((tmpFile) || ((!tmpFile) && (m_AutoRGNDataList[i]->GetColorCode() == CTC)))
			{
				count++;
				CString singleLine;
				int x0, y0;
				m_AutoRGNDataList[i]->GetPosition(&x0, &y0);
				singleLine.Format(_T("0 1, 1 %lu, 2 %d %d, 3 0 0, 4 0, 5 1, 6 2 100 100, 7 %d\n"),
					m_AutoRGNDataList[i]->GetColorCode(), x0, y0, count);
				theFile.WriteString(singleLine);
			}
		}
		theFile.Close();
	}
}

void CCTCImageGalleryDlg::SaveTempRegionFile()
{
	CString filename = _T("C:\\CTCReviewerLog\\CTCImageGalleryV3Temp.tmp");
	SaveRegionFile(filename);
	CString message;
	message.Format(_T("CTCImageGalleryV3Temp.tmp file saved for %s at Page No.%d"), m_RegionFilename, m_CurrentPage);
	m_Log.Message(message);
}

void CCTCImageGalleryDlg::RankByScore()
{
	for (int i = 0; i < (int)m_AutoRGNDataList.size(); i++)
	{
		CRGNData *ptr = m_AutoRGNDataList[i];
		int x0, y0;
		ptr->GetPosition(&x0, &y0);
		unsigned short *redImg = new unsigned short[m_ImageWidth * m_ImageHeight];
		unsigned short *greenImg = new unsigned short[m_ImageWidth * m_ImageHeight];
		unsigned short *blueImg = new unsigned short[m_ImageWidth * m_ImageHeight];
		bool redRet = m_RedTIFFData->GetImageRegion(x0, y0, m_ImageWidth, m_ImageHeight, redImg);
		bool greenRet = m_GreenTIFFData->GetImageRegion(x0, y0, m_ImageWidth, m_ImageHeight, greenImg);
		bool blueRet = m_BlueTIFFData->GetImageRegion(x0, y0, m_ImageWidth, m_ImageHeight, blueImg);
		if (redRet && greenRet && blueRet)
		{
			ptr->SetImages(redImg, greenImg, blueImg);
			ProcessOneRegionForDisplay(ptr);
		}
		else
		{
			ptr->SetScore(0);
			CString message;
			message.Format(_T("Failed to load region image: X0=%d,Y0=%d,Width=%d,Height=%d"),
				x0, y0, m_ImageWidth, m_ImageHeight);
			m_Log.Message(message);
			memset(redImg, 0, sizeof(unsigned short) * m_ImageWidth * m_ImageHeight);
			memset(greenImg, 0, sizeof(unsigned short) * m_ImageWidth * m_ImageHeight);
			memset(blueImg, 0, sizeof(unsigned short) * m_ImageWidth * m_ImageHeight);
			ptr->SetImages(redImg, greenImg, blueImg);
			CopyToRGBImage(ptr, i);
		}
	}
	if ((int)m_AutoRGNDataList.size() > 0)
		QuickSort(&m_AutoRGNDataList, 0, (int)m_AutoRGNDataList.size() - 1);
}

template<typename T>
void CCTCImageGalleryDlg::QuickSort(vector<T> *list, int lo, int hi)
{
	if (lo < hi)
	{
		int position = Partition(list, lo, hi);
		QuickSort(list, lo, position - 1);
		QuickSort(list, position + 1, hi);
	}
}

int CCTCImageGalleryDlg::Partition(vector<CRGNData *> *list, int lo, int hi)
{
	int position = ChoosePivot(list, lo, hi);
	int score = (*list)[position]->GetScore();
	Swap(list, position, hi);
	int storeIndex = lo;
	for (int i = lo; i < hi; i++)
	{
		if ((*list)[i]->GetScore() > score)
		{
			Swap(list, i, storeIndex);
			storeIndex++;
		}
	}
	Swap(list, storeIndex, hi);
	return storeIndex;
}

int CCTCImageGalleryDlg::ChoosePivot(vector<CRGNData *> *list, int lo, int hi)
{
	int pivot = lo;
	int score0 = (*list)[lo]->GetScore();
	int score1 = (*list)[(lo + hi) / 2]->GetScore();
	int score2 = (*list)[hi]->GetScore();
	if ((score0 >= score1) && (score1 >= score2))
		pivot = (lo + hi) / 2;
	else if ((score0 >= score2) && (score2 >= score1))
		pivot = hi;
	else
		pivot = lo;
	return pivot;
}

template<typename T>
void CCTCImageGalleryDlg::Swap(vector<T> *list, int index1, int index2)
{
	T ptr = (*list)[index1];
	(*list)[index1] = (*list)[index2];
	(*list)[index2] = ptr;
}
