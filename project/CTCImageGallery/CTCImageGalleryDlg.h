
// CTCImageGalleryDlg.h : header file
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "RGNData.h"
#include "Log.h"
#include <vector>
#include "HitFindingOperation.h"
#include "CTCParams.h"
#include "SingleChannelTIFFData.h"
#include "CTCImageGallery.h"
#include "WaitDialog.h"

using namespace std;

#define NUM_IMAGE_COLUMNS 8
#define NUM_IMAGE_ROWS	4
#define NUM_IMAGES	(NUM_IMAGE_COLUMNS * NUM_IMAGE_ROWS) 

// CCTCImageGalleryDlg dialog
class CCTCImageGalleryDlg : public CDialogEx
{
// Construction
public:
	CCTCImageGalleryDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_CTCIMAGEGALLERY_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;
	CImage m_Image;
	CSingleChannelTIFFData *m_RedTIFFData;
	CSingleChannelTIFFData *m_GreenTIFFData;
	CSingleChannelTIFFData *m_BlueTIFFData;
	void FreeRGNList(vector<CRGNData *> *rgnList);
	void UpdateImageDisplay();
	vector<CRGNData *> m_AutoRGNDataList;
	void DisplayCellRegions();
	void CopyToRGBImage(CRGNData *rgnPtr, int ImageLocationIndex);
	BYTE GetContrastEnhancedByte(unsigned short value, unsigned short contrast, int cutoff);
	CHitFindingOperation m_HitFinder;
	int m_RedIntensity;
	int m_GreenIntensity;
	int m_BlueIntensity;
	CCTCParams m_CTCParams;
	int m_ImageWidth;
	int m_ImageHeight;
	void PaintCImages();
	bool LoadRegionData(CString filename);
	void ProcessOneRegionForDisplay(CRGNData *ptr);
	void ResetAllCounts();
	void SaveRegionFile(CString filename);
	void SaveTempRegionFile();
	void EnableButtons(BOOL enable);
	template<typename T> void QuickSort(vector<T> *list, int lo, int hi);
	template<typename T> void Swap(vector<T> *list, int index1, int index2);
	int Partition(vector<CRGNData *> *list, int lo, int hi);
	int ChoosePivot(vector<CRGNData *> *list, int lo, int hi);

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedLoadimg();
	afx_msg void OnBnClickedLoadrgn();
	afx_msg void OnBnClickedNextview();
	afx_msg void OnBnClickedSavergn();
	CString m_ImageFilename;
	CString m_RegionFilename;
	CLog m_Log;
	int m_TotalRegions;
	afx_msg void OnBnClickedCancel();
	CStatic m_HitImage;
	int m_TotalPages;
	int m_CurrentPage;
	int m_TotalSelected;
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedPrevview();
	afx_msg void OnBnClickedGoto();
	BOOL PreTranslateMessage(MSG* pMsg);
	CButton m_BlueColor;
	CButton m_GreenColor;
	CButton m_RedColor;
	CButton m_ShowBoundary;
	CButton m_NotShowBoundary;
	afx_msg void OnBnClickedRedcolor();
	afx_msg void OnBnClickedGreencolor();
	afx_msg void OnBnClickedBluecolor();
	afx_msg void OnBnClickedShow();
	afx_msg void OnBnClickedNotshow();
	CWaitDialog *m_WaitDialog;
	void RankByScore();
};
